﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterServices
{
    internal class Helper
    {
        public void CreateFolderIfNotExist(string path)
        {
            var exists = Directory.Exists(path);

            if (exists) return;

            Directory.CreateDirectory(path);
        }

        public void CreateFileTik(string path)
        {
            using (StreamWriter sw = File.CreateText(path))
            {
                sw.WriteLine("0");
                
            }
            
        }

        private static bool IsFileLocked(string filename)
        {
            var Locked = false;
            FileStream fs = null;

            try
            {
                fs =
                    File.Open(filename, FileMode.OpenOrCreate,
                        FileAccess.ReadWrite, FileShare.None);
                fs.Close();
            }
            catch (IOException ex)
            {
                Locked = true;
            }
            finally
            {
                if (fs != null) fs.Close();
            }

            return Locked;
        }

        public bool IsOdd(int value)
        {
            return value % 2 != 0;
        }
    }
}
