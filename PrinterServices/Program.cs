﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Media;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Timer = System.Timers.Timer;
using System.ServiceProcess;



namespace PrinterServices
{
    class Program
    {
        
        public static Timer aTimerSync;
        public static Timer aTimerPrint;
        public static Timer aTimerListenPrinter;
        public static Timer aTimerBackup;
        public static DateTime localDate = DateTime.Now;
        public static CultureInfo culture = new CultureInfo("en-US");
        public static string now = localDate.ToString(culture);
        public static Helper help = new Helper();


        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);
        public delegate bool HandlerRoutine(CtrlTypes CtrlType);
        private static bool isclosing = false;
        public static String MERCHANT_ID = ConfigurationSettings.AppSettings["MERCHANT_ID"].ToString();
        public static String MERCHANT_KEY = ConfigurationSettings.AppSettings["MERCHANT_KEY"].ToString();
        public static String INTERVAL = ConfigurationSettings.AppSettings["INTERVAL"].ToString();
        public static String MAIN_PRINTER = ConfigurationSettings.AppSettings["MAIN_PRINTER"].ToString();
        public static String DOCKET_PRINTER = ConfigurationSettings.AppSettings["DOCKET_PRINTER"].ToString();
        public static String EXCEPT_SERVICES = ConfigurationSettings.AppSettings["EXCEPT_SERVICES"].ToString();
        public static String PRINTER_LOCATION = ConfigurationSettings.AppSettings["PRINTER_LOCATION"].ToString();
        public static String PRINT_MODE = ConfigurationSettings.AppSettings["PRINT_MODE"].ToString();
        public static bool REPRINT_RECEIPT = ConfigurationSettings.AppSettings["REPRINT_RECEIPT"].ToString() == "1" ? true : false;



        public static PCPrint StartPrint = new PCPrint();

        public static int count = 0;

        public static log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(Program));
        public static string tik = System.Environment.CurrentDirectory + "\\tik.json";

        public static FileInfo[] folderSelected;


        static void Main(string[] args)
        {
            BootLoader();

            runService();

            SetConsoleCtrlHandler(new HandlerRoutine(ConsoleCtrlCheck), true);

            while (!isclosing) ;
            
            Console.ReadLine();

        }

        public static void runService()
        {
            
            aTimerPrint = new Timer(Convert.ToInt32(INTERVAL));
            aTimerPrint.Elapsed += PrintAllPrinters;
            aTimerPrint.AutoReset = true;
            aTimerPrint.Enabled = true;

        }

        public static bool serviceExists(string ServiceName)
        {
            return ServiceController.GetServices().Any(serviceController => serviceController.ServiceName.Equals(ServiceName));
        }
        public static void checkServices()
        {
            String[] services = EXCEPT_SERVICES.Split(',');

            foreach (var a in services)
            {
                if (serviceExists(a))
                {
                    Console.WriteLine("This application was running on Service.");
                    Console.WriteLine("will be closed in 5 minutes ...");

                    Thread.Sleep(5000);
                    System.Environment.Exit(1);
                    break;
                }
                
            }
        }

        public static void buildMainPrinters()
        {
            String[] Printers = MAIN_PRINTER.Split(',');

            help.CreateFolderIfNotExist(PRINTER_LOCATION);


            foreach (var p in Printers)
            {
                help.CreateFolderIfNotExist(PRINTER_LOCATION + p);
            }

        }

        public static void buildDocketPrinters()
        {
            String[] DocketPrinters = DOCKET_PRINTER.Split(',');

            foreach (var p in DocketPrinters)
            {
                help.CreateFolderIfNotExist(PRINTER_LOCATION+"KITCHEN/" + p);
            }

        }

        public static string getPrintMode()
        {
            string res = string.Empty;

            switch (PRINT_MODE)
            {
                case "1":
                    res = "Print Receipt and Docket";
                    break;

                case "2":
                    res = "Print Receipt Only";
                    break;

                case "3":
                    res = "Print Docket Only";
                    break;
                default:
                    res = "Undefined Mode";
                    break;

            }

            _logger.Info(string.Concat("======> ","Print Mode ", PRINT_MODE));
            Console.WriteLine(string.Concat("======> ","Print Mode ", PRINT_MODE));
            _logger.Info(string.Concat("======> ", res));
            Console.WriteLine(string.Concat("======> ", res));

            return res;
        }
        public static void BootLoader()
        {
            try
            {
                checkServices();
                
                buildMainPrinters();

                buildDocketPrinters();
                
                help.CreateFileTik(tik);

                getPrintMode();
                

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
        }
        
        private static bool IsFileLocked(string filename)
        {
            var Locked = false;
            FileStream fs = null;

            try
            {
                fs =
                    File.Open(filename, FileMode.OpenOrCreate,
                        FileAccess.ReadWrite, FileShare.None);
                fs.Close();
            }
            catch (IOException ex)
            {
                Locked = true;
            }
            finally
            {
                if (fs != null) fs.Close();
            }

            return Locked;
        }

        public static void printQr()
        {
            var QRCODE = new DirectoryInfo(PRINTER_LOCATION + "QRCODE");
            var JsonQr = QRCODE.GetFiles("*.json");
            var ImgQr = QRCODE.GetFiles("*.png");
            var iQr = 0;
            
            if(JsonQr.Count() > 0)
            {
                foreach (var file in JsonQr)
                {
                    var fileZ = @"C:\log\printer\QRCODE\" + file.Name;
                    var fileName = file.Name.Split('.');
                    var qrFile = @"C:\log\printer\QRCODE\" + fileName[0].Trim() + ".png";

                    try
                    {

                        using (var r = new StreamReader(fileZ))
                        {
                            var Fjson = r.ReadToEnd();

                            var parsed = JObject.Parse(Fjson);

                            r.Close();

                            var doPrint = new PCPrint();

                            doPrint.PrintQR(parsed, fileZ, qrFile);

                            Thread.Sleep(200);


                        }

                    }
                    catch (Exception ty)
                    {
                        Console.WriteLine("ERROR PRINT QR : " + ty.Message);

                    }

                    iQr++;
                }

            }
            
        }

        public static void printGeneral()
        {
            var GENERAL = new DirectoryInfo(@"C:\log\printer\GENERAL");
            var FilesGENERAL = GENERAL.GetFiles("*.json");
            var iGeneral = 0;
            
            if (FilesGENERAL.Count() > 0)
            {
                foreach (var file in FilesGENERAL)
                {
                    var fileZ = @"C:\log\printer\GENERAL\" + file.Name;

                    using (var r = new StreamReader(fileZ))
                    {
                        var Fjson = r.ReadToEnd();

                        var parsed = JObject.Parse(Fjson);

                        r.Close();

                        var doPrint = new PCPrint();

                        doPrint.PrintCallWaiter(parsed, fileZ);

                        Thread.Sleep(200);

                    }


                    iGeneral++;
                }

            }

        }

        public static void printPaid()
        {
            var PAID = new DirectoryInfo(PRINTER_LOCATION + "PAID");
            folderSelected = PAID.GetFiles("*.json");
            
            if (folderSelected.Count() > 0)
            {
                var i = 0;

                foreach (var a in folderSelected)
                {
                    var file = PRINTER_LOCATION+"\\PAID\\" + a.Name;
                    
                    try
                    {
                        using (var r = new StreamReader(file))
                        {
                            
                            var parsed = JObject.Parse(r.ReadToEnd());

                            r.Close();

                            StartPrint.dataCollect(parsed);

                            switch (PRINT_MODE)
                            {
                                case "1":
                                    
                                    StartPrint.PrintReceipt(parsed);
                                        
                                    StartPrint.SplitReceipt(parsed); //Kitchen Printer
                                    
                                    break;
                                case "2":
                                    StartPrint.PrintReceipt(parsed);
                                    
                                    break;
                                case "3":
                                    StartPrint.SplitReceipt(parsed); //Kitchen Printer
                                    
                                    break;
                                default:
                                    return;
                                    break;

                            }
                            
                            deleteFile(file);

                        }

                    }
                    catch(Exception uy)
                    {
                        var LineNumber = new StackTrace(uy, true).GetFrame(0).GetFileLineNumber();

                        Console.WriteLine("Error at line :" + LineNumber + " ==>" + uy.Message + "\n");
                        
                        deleteFile(file);
                    }
                    
                    i++;
                }
            }
            
        }

        public static void printEmenu()
        {
            var EMENU = new DirectoryInfo(PRINTER_LOCATION+"EMENU");
            folderSelected = EMENU.GetFiles("*.json");
            
            if (folderSelected.Count() > 0)
            {
                var i = 0;

                foreach (var a in folderSelected)
                {
                    var file = PRINTER_LOCATION + "\\EMENU\\" + a.Name;

                    try
                    {
                        using (var r = new StreamReader(file))
                        {

                            var parsed = JObject.Parse(r.ReadToEnd());

                            r.Close();

                            StartPrint.dataCollect(parsed);

                            switch (PRINT_MODE)
                            {
                                case "1":

                                    StartPrint.PrintReceipt(parsed);

                                    StartPrint.SplitReceipt(parsed); //Kitchen Printer

                                    break;
                                case "2":
                                    StartPrint.PrintReceipt(parsed);

                                    break;
                                case "3":
                                    StartPrint.SplitReceipt(parsed); //Kitchen Printer

                                    break;
                                default:
                                    return;
                                    break;

                            }

                            deleteFile(file);

                        }

                    }
                    catch (Exception uy)
                    {
                        var LineNumber = new StackTrace(uy, true).GetFrame(0).GetFileLineNumber();

                        Console.WriteLine("Error at line :" + LineNumber + " ==>" + uy.Message + "\n");

                        deleteFile(file);
                    }

                    i++;
                }
            }
        }

        public static void printCash()
        {
            var CASH = new DirectoryInfo(PRINTER_LOCATION+"CASH");
            var FilesCASH = CASH.GetFiles("*.json");
            var iCash = 0;
            
            if (FilesCASH.Count() > 0)
            {
                try
                {
                    foreach (var file in FilesCASH)
                    {
                        var fileX = @"C:\log\printer\CASH\" + file.Name; //filter item for only label

                        using (var r = new StreamReader(fileX))
                        {
                            var Fjson = r.ReadToEnd();

                            var parsed = JObject.Parse(Fjson);

                            r.Close();

                            var doPrint = new PCPrint();

                            //doPrint.PrintReceipt(parsed, fileX, ""); //CASHAC or UNPAID ORDER
                            //Thread.Sleep(200);

                            doPrint.SplitReceipt(parsed); //Kitchen Printer
                            Thread.Sleep(200);

                            if (File.Exists(fileX))
                                if (!IsFileLocked(fileX))
                                    File.Delete(fileX);
                            Thread.Sleep(200);
                        }


                        iCash++;
                    }

                }
                catch (Exception ui)
                {
                    Console.WriteLine(ui.Message);
                }


            }

        }

        public static void printReceiptAndLabel()
        {
            //CASH =>RECEIPT AND LABEL

            var TM30 = new DirectoryInfo(PRINTER_LOCATION + "TM30");
            folderSelected = TM30.GetFiles("*.json");
            
            if (folderSelected.Count() > 0)
            {
                var i = 0;

                foreach (var a in folderSelected)
                {
                    var file = PRINTER_LOCATION + "\\TM30\\" + a.Name;

                    try
                    {
                        using (var r = new StreamReader(file))
                        {

                            var parsed = JObject.Parse(r.ReadToEnd());

                            r.Close();

                            StartPrint.dataCollect(parsed);

                            switch (PRINT_MODE)
                            {
                                case "1":

                                    StartPrint.PrintReceipt(parsed);

                                    StartPrint.SplitReceipt(parsed); //Kitchen Printer

                                    break;
                                case "2":
                                    StartPrint.PrintReceipt(parsed);

                                    break;
                                case "3":
                                    StartPrint.SplitReceipt(parsed); //Kitchen Printer

                                    break;
                                default:
                                    return;
                                    break;

                            }

                            deleteFile(file);

                        }

                    }
                    catch (Exception uy)
                    {
                        var LineNumber = new StackTrace(uy, true).GetFrame(0).GetFileLineNumber();

                        Console.WriteLine("Error at line :" + LineNumber + " ==>" + uy.Message + "\n");

                        deleteFile(file);
                    }

                    i++;
                }
            }

        }

        public static void printReprint()
        {
            var RECEIPT = new DirectoryInfo(PRINTER_LOCATION + "RECEIPT");
            folderSelected = RECEIPT.GetFiles("*.json");

            
            if (folderSelected.Count() > 0)
            {
                var i = 0;

                foreach (var a in folderSelected)
                {
                    var file = PRINTER_LOCATION + "\\RECEIPT\\" + a.Name;

                    try
                    {
                        if (REPRINT_RECEIPT)
                        {
                            using (var r = new StreamReader(file))
                            {

                                var parsed = JObject.Parse(r.ReadToEnd());

                                r.Close();

                                StartPrint.dataCollect(parsed);

                                StartPrint.PrintReceipt(parsed);

                                deleteFile(file);
                            }
                        }
                        else
                        {
                            deleteFile(file);
                        }
                        

                    }
                    catch (Exception uy)
                    {
                        var LineNumber = new StackTrace(uy, true).GetFrame(0).GetFileLineNumber();

                        Console.WriteLine("Error at line :" + LineNumber + " ==>" + uy.Message + "\n");

                        deleteFile(file);
                    }

                    i++;
                }
            }

        }

        public static void printLabel()
        {
            //PRINT LABEL ONLY
            var CLabel = new DirectoryInfo(@"C:\log\printer\LABEL");
            var Files = CLabel.GetFiles("*.json");
            var iLabel = 0;

            
            if (Files.Count() > 0)
            {
                //read file
                foreach (var file in Files)
                {
                    var fileZ = @"C:\log\printer\LABEL\" + file.Name; //filte item for only label

                    using (var r = new StreamReader(fileZ))
                    {
                        var Fjson = r.ReadToEnd();

                        var parsed = JObject.Parse(Fjson);

                        r.Close();

                        //loop order items
                        var paymentType = parsed["orders"]["orderPayments"].ToString();

                        var a = JArray.Parse(paymentType);

                        foreach (var o in a.Children<JObject>())
                        {
                            var payment = o["payment_type"].ToString();

                            var doPrint = new PCPrint();

                            //doPrint.PrintLabelItem(parsed, fileZ); //LABEL ORDER ONLY PRINT ITEMS


                            //DELETE FILE RECEIPT
                            if (File.Exists(fileZ))
                                if (!IsFileLocked(fileZ))
                                    File.Delete(fileZ);
                            Thread.Sleep(500);
                        }
                    }


                    iLabel++;
                }

            }
            
        }

        public static void printError()
        {
            //PRINT ERROR PRINT ONLY
            var C = new DirectoryInfo(@"C:\log\printer\FAILED");
            var Faileds = C.GetFiles("*.json");
            var iFailed = 0;
            

            //read file
            if (Faileds.Count() > 0)
            {
                foreach (var file in Faileds)
                {
                    var fileX = file.DirectoryName + "\\" + file.Name;
                    var fx = "";

                    using (var r = new StreamReader(fileX))
                    {
                        var Fjson = r.ReadToEnd();

                        var parsed = JObject.Parse(Fjson);

                        r.Close();

                        var doPrint = new PCPrint();

                        doPrint.PrintReceiptFailed(parsed, fileX, fx);
                        Thread.Sleep(200);
                    }

                    iFailed++;
                }

            }
            
        }
        private static  void PrintAllPrinters(object source, ElapsedEventArgs e)
        {

            try
            {
                
                if (File.Exists(tik))
                {
                    
                    string text = File.ReadAllText(tik);
                    
                    if (text.Trim() == "0")
                    {
                        //Console.WriteLine("Checking..."+count++);
                        
                        File.WriteAllText(tik, "1");

                        printPaid();

                        printQr();

                        printEmenu();

                        printGeneral();
                        
                        printCash();

                        printReceiptAndLabel();

                        printReprint();

                        printLabel();

                        printError();
                        
                        
                        var printKitchen = new PCPrint();

                        printKitchen.PrintToKitchen();
                        
                        File.WriteAllText(tik, "0");//return back the tick


                    }

                }
                
            }
            catch (Exception ty)
            {
                var LineNumber = new StackTrace(ty, true).GetFrame(0).GetFileLineNumber();

                Console.WriteLine("Error at line :"+LineNumber +" ==>"+ty.Message);

                File.WriteAllText(tik, "0");

                if (File.Exists(tik))
                {
                    string text = File.ReadAllText(tik);
                    if (text == "1")
                    {
                        File.WriteAllText(tik, "0");

                    }
                }
                
            }
            
        }

        public static void deleteFile(String file)
        {
            try
            {
                if (File.Exists(file))
                {
                    while (true)
                    {
                        if (!IsFileLocked(file))
                        {
                            File.Delete(file);
                            break;
                        }
                        
                    }
                }

            }
            catch(Exception t)
            {
                Console.WriteLine(t.Message);
            }
            
        }

        public enum CtrlTypes

        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }

        private static bool ConsoleCtrlCheck(CtrlTypes ctrlType)

        {
            switch (ctrlType)
            {

                case CtrlTypes.CTRL_C_EVENT:

                    isclosing = true;

                    Console.WriteLine("CTRL+C received!");

                    break;

                case CtrlTypes.CTRL_BREAK_EVENT:

                    isclosing = true;

                    Console.WriteLine("CTRL+BREAK received!");

                    break;

                case CtrlTypes.CTRL_CLOSE_EVENT:

                    isclosing = true;

                    break;

                case CtrlTypes.CTRL_LOGOFF_EVENT:

                case CtrlTypes.CTRL_SHUTDOWN_EVENT:

                    isclosing = true;

                    break;

            }

            return true;

        }
    }
}
