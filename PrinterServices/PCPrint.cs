﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Text;
using System.Threading;
using PrinterServices;
using System.Configuration;
using Zen.Barcode;

namespace PrinterServices
{
    public class PCPrint : PrintDocument
    {
        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern bool about();

        [DllImport("kernel32.dll", CharSet = CharSet.None, ExactSpelling = false, SetLastError = true)]
        private static extern bool AllocConsole();

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern int barcode(string x, string y, string type, string height, string readable, string rotation, string narrow, string wide, string code);

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern int clearbuffer();

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern int closeport();

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern int downloadpcx(string filename, string image_name);

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern int sendcommand(string printercommand);

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern int setup(string width, string height, string speed, string density, string sensor, string vertical, string offset);

        [DllImport("user32.dll", CharSet = CharSet.None, ExactSpelling = false)]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern int formfeed();

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern int nobackfeed();

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern bool openport(string printer);

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern int printerfont(string x, string y, string fonttype, string rotation, string xmul, string ymul, string text);

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern int printlabel(string set, string copy);
        
        [DllImport("kernel32.dll", CharSet = CharSet.None, ExactSpelling = false)]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("winspool.drv", CharSet = CharSet.Auto, ExactSpelling = false, SetLastError = true)]
        public static extern bool GetDefaultPrinter(StringBuilder pszBuffer, ref int size);

        private const int SW_HIDE = 0;

        private const int SW_SHOW = 5;

        public static DateTime localDate;

        public static CultureInfo culture;

        public static string now;

        private readonly log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(Program));
        
        public static string myIp;
        
        public JObject fileReceipt = new JObject();

        public string QRFile;

        public static String MERCHANT_NAME = ConfigurationSettings.AppSettings["MERCHANT_NAME"].ToString();

        public static String MASTER_PRINTER = ConfigurationSettings.AppSettings["MASTER_PRINTER"].ToString();

        public static String QR_PRINTERS = ConfigurationSettings.AppSettings["QR_PRINTERS"].ToString();

        public static String DOCKET_PRINTER = ConfigurationSettings.AppSettings["DOCKET_PRINTER"].ToString();

        public bool IS_BUZZER = ConfigurationSettings.AppSettings["SHOW_TABLE"].ToString() == "1" ? true : false;

        public bool IS_SHOW_TERMINAL = ConfigurationSettings.AppSettings["SHOW_TERMINAL"].ToString() == "1" ? true : false;

        public bool IS_SHOW_ORDER_TYPE = ConfigurationSettings.AppSettings["SHOW_ORDER_TYPE"].ToString() == "1" ? true : false;

        public bool IS_SHOW_ORDER_NO = ConfigurationSettings.AppSettings["SHOW_ORDER_NO"].ToString() == "1" ? true : false;

        public bool FIELD_TABLE = ConfigurationSettings.AppSettings["FIELD_TABLE"].ToString() == "table_no" ? true : false;

        public bool IS_SHOW_CUSTOMER = ConfigurationSettings.AppSettings["SHOW_CUSTOMER"].ToString() == "1" ? true : false;

        public bool IS_SHOW_PHONE = ConfigurationSettings.AppSettings["SHOW_PHONE"].ToString() == "1" ? true : false;

        public bool IS_SHOW_PRINTER = ConfigurationSettings.AppSettings["SHOW_PRINTER_NAME"].ToString() == "1" ? true : false;

        public bool IS_SHOW_BARCODE = ConfigurationSettings.AppSettings["SHOW_BARCODE"].ToString() == "1" ? true : false;
        
        public bool IS_SEND_IFTTT = ConfigurationSettings.AppSettings["SEND_INFO_IFTTT"].ToString() == "1" ? true : false;

        public static string START_Y_CONTENT_RECEIPT = ConfigurationSettings.AppSettings["START_Y_CONTENT_RECEIPT"];

        public static string START_Y_CONTENT_DOCKET = ConfigurationSettings.AppSettings["START_Y_CONTENT_DOCKET"];

        public static string START_X_POST_PRICE = ConfigurationSettings.AppSettings["START_X_POST_PRICE"];

        public static string PADDING_TOP_DOCKET = ConfigurationSettings.AppSettings["PADDING_TOP_DOCKET"];
        
        public bool IS_SHOW_CENT = ConfigurationSettings.AppSettings["SHOW_CENT"].ToString() == "1" ? true : false;

        public bool IS_SHOW_GST = ConfigurationSettings.AppSettings["SHOW_GST"].ToString() == "1" ? true : false;

        public bool IS_SHOW_TAX = ConfigurationSettings.AppSettings["SHOW_TAX"].ToString() == "1" ? true : false;

        public bool IS_SHOW_SERVICE_CHARGE = ConfigurationSettings.AppSettings["SHOW_SERVICE_CHARGE"].ToString() == "1" ? true : false;

        public bool IS_SHOW_ROUNDING = ConfigurationSettings.AppSettings["SHOW_ROUNDING"].ToString() == "1" ? true : false;

        public bool IS_SHOW_THANKS = ConfigurationSettings.AppSettings["SHOW_THANKS"].ToString() == "1" ? true : false;

        public bool IS_TOTAL_AS_TEXT = ConfigurationSettings.AppSettings["TOTAL_AS_TEXT"].ToString() == "1" ? true : false;

        public static float FONT_SIZE =float.Parse(ConfigurationSettings.AppSettings["FONT_DEFAULT_SIZE"]);

        public static float FONT_DOCKET_CONTENT_SIZE = float.Parse(ConfigurationSettings.AppSettings["FONT_DOCKET_CONTENT_SIZE"]);

        public static float FONT_DOCKET_ORDER_SIZE = float.Parse(ConfigurationSettings.AppSettings["FONT_DOCKET_ORDER_SIZE"]);

        public static int COUNT_RECEIPT = Convert.ToInt32(ConfigurationSettings.AppSettings["COUNT_PRINT_RECEIPT"]);

        public static int COUNT_DOCKET = Convert.ToInt32(ConfigurationSettings.AppSettings["COUNT_PRINT_DOCKET"]);

        public Font DefaultFont = new Font("Tahoma", FONT_SIZE);

        NumberFormatInfo nfi = new NumberFormatInfo { NumberGroupSeparator = ",", NumberDecimalDigits = 0 };

        
        public bool reprint;

        public string queue;

        public string orderType;

        public string printerName;

        public int count = 0;//this variable is for total number of items in the list or array

        public int ix = 0;//this variable is  for no of item per page

        private Font _font;

        private string _text;

        protected override bool CanRaiseEvents
        {
            get
            {
                return base.CanRaiseEvents;
            }
        }

        public Font Font
        {
            get;
            private set;
        }

        public Font PrinterFont
        {
            get
            {
                return this._font;
            }
            set
            {
                this._font = value;
            }
        }

        public override ISite Site
        {
            get
            {
                return base.Site;
            }
            set
            {
                base.Site = value;
            }
        }

        public string TextToPrint
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
            }
        }

        static PCPrint()
        {
            PCPrint.localDate = DateTime.Now;
            PCPrint.culture = new CultureInfo("en-US");
            PCPrint.now = PCPrint.localDate.ToString(PCPrint.culture);
            PCPrint.myIp = PCPrint.GetLocalIPAddress();
        }

        public PCPrint()
        {
            this._text = string.Empty;
        }

        public PCPrint(string str)
        {
            this._text = str;
        }

        
        public override ObjRef CreateObjRef(Type requestedType)
        {
            return base.CreateObjRef(requestedType);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static string GetLocalIPAddress()
        {
            IPAddress[] addressList = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
            for (int i = 0; i < (int)addressList.Length; i++)
            {
                IPAddress ip = addressList[i];
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
        
        public bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any<string>();
        }

        public bool IsFileLocked(string filename)
        {
            bool Locked = false;
            FileStream fs = null;
            try
            {
                try
                {
                    fs = File.Open(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                    fs.Close();
                }
                catch (IOException oException)
                {
                    Locked = true;
                }
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
            return Locked;
        }

        public bool IsNullOrEmpty(Array array)
        {
            return array == null;
        }

        
        protected override void OnBeginPrint(PrintEventArgs e)
        {
            base.OnBeginPrint(e);
            if (this._font == null)
            {
                this._font = new Font("Tahoma", 10f);
            }
        }

        protected override void OnEndPrint(PrintEventArgs e)
        {
            base.OnEndPrint(e);
        }

        protected override void OnQueryPageSettings(QueryPageSettingsEventArgs e)
        {
            base.OnQueryPageSettings(e);
        }
        
        private static void PostJson(string uri, template postParameters)
        {
            string postData = JsonConvert.SerializeObject(postParameters);
            byte[] bytes = Encoding.UTF8.GetBytes(postData);
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentLength = (long)((int)bytes.Length);
            httpWebRequest.ContentType = "application/json";
            try
            {
                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Count<byte>());
                }
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpWebResponse.StatusCode != HttpStatusCode.OK)
                {
                    string message = string.Format("POST failed. Received HTTP {0}", httpWebResponse.StatusCode);
                    throw new ApplicationException(message);
                }
            }
            catch (Exception exception)
            {
                var LineNumber = new StackTrace(exception, true).GetFrame(0).GetFileLineNumber();

                Console.WriteLine("Error at line :" + LineNumber + " ==>" + exception.Message);

            }
        }

       
        public void PrintLabelItem(JObject json, string fileY)
        {
            CultureInfo culture = new CultureInfo("en-US");
            PCPrint.localDate.ToString(culture);
            string mainOrders = json["orders"]["orderItemDetails"].ToString();
            JArray aX = JArray.Parse(mainOrders);
            int mainQty = 0;
            foreach (JObject ob in aX.Children<JObject>())
            {
                mainQty += int.Parse(ob["quantity"].ToString());
            }
            string dJson = json["orders"]["orderItemDetails"].ToString();
            JArray a = JArray.Parse(dJson);
            int inc = 55;
            int item = 1;
            foreach (JObject o in a.Children<JObject>())
            {
                int max = int.Parse(o["quantity"].ToString());
                for (int i = 1; i < max + 1; i++)
                {
                    try
                    {
                        PCPrint.openport("TDP225");
                        PCPrint.setup("32", "25", "12", "8", "0", "3", "0");
                        PCPrint.clearbuffer();
                        PCPrint.windowsfont(20, 5, 24, 0, 2, 0, " Tahoma", json["merchant"]["name"].ToString());
                        object[] str = new object[] { "(", json["orders"]["queue_no"], " - ", null, null, null, null };
                        DateTime now = DateTime.Now;
                        str[3] = now.ToString("dd/MM");
                        str[4] = " ";
                        now = DateTime.Now;
                        str[5] = now.ToString("HH:mm");
                        str[6] = ")";
                        PCPrint.windowsfont(20, 30, 24, 0, 0, 0, " Tahoma", string.Concat(str));
                        JObject jo = JObject.Parse((string.IsNullOrEmpty(o["skus"].ToString()) ? "" : o["skus"].ToString()));
                        string plu = (string.IsNullOrEmpty(o["skus"].ToString()) ? "" : jo["plu"].ToString());
                        PCPrint.windowsfont(20, 55, 24, 0, 2, 0, " Tahoma", plu);
                        foreach (JToken x in (IEnumerable<JToken>)o["orderCustomizationItemDetails"])
                        {
                            foreach (JToken y in (IEnumerable<JToken>)x["orderCustomizationOptionItemDetails"])
                            {
                                inc += 23;
                                string skusOption = y["customizationOptions"].ToString();
                                JObject joOption = JObject.Parse(skusOption);
                                string pluOption = joOption["plu"].ToString();
                                PCPrint.windowsfont(30, inc, 20, 0, 0, 0, " Tahoma", pluOption);
                            }
                            inc = 55;
                        }
                        PCPrint.windowsfont(12, 170, 17, 0, 1, 0, " Tahoma", "* Best consumed within 30 mins");
                        PCPrint.windowsfont(225, 170, 17, 0, 1, 0, " Tahoma", string.Concat(new object[] { "(", item, "/", mainQty, ")" }));
                        PCPrint.printlabel("1", "1");
                        PCPrint.closeport();
                        item++;
                    }
                    catch (InvalidPrinterException invalidPrinterException)
                    {
                        var LineNumber = new StackTrace(invalidPrinterException, true).GetFrame(0).GetFileLineNumber();

                        Console.WriteLine("Error at line :" + LineNumber + " ==>" + invalidPrinterException.Message);
                        /*
                        InvalidPrinterException e = invalidPrinterException;
                        PCPrint.PostJson("https://maker.ifttt.com/trigger/backend_alert/with/key/c4VTKqDv6MkrNGjvQISqAh", new template()
                        {
                            value1 = "Taigai Printer Services",
                            value2 = "Label's Printer",
                            value3 = e.Message
                        });
                        return;
                        */
                    }
                }
            }
            if (File.Exists(fileY) && !this.IsFileLocked(fileY))
            {
                File.Delete(fileY);
            }
        }

        private Image getBarcode(string str)
        {
            BarcodeSymbology s = BarcodeSymbology.Code128;
            BarcodeDraw drawObject = BarcodeDrawFactory.GetSymbology(s);
            var metrics = drawObject.GetDefaultMetrics(60);
            metrics.Scale = 2;
            var barcodeImage = drawObject.Draw(str, metrics);
            return barcodeImage;
        }

        public void PrintReceipt(JObject json)
        {
            try
            {
                
                if (this.fileReceipt.ContainsKey("orders"))
                {
                    this.fileReceipt = json;

                    Console.WriteLine("\n");

                    this._logger.Info(string.Concat("Printer Name  : ", MASTER_PRINTER));
                    Console.WriteLine(string.Concat("Printer Name  : ", MASTER_PRINTER));

                    this._logger.Info(string.Concat("Order No      : ", this.getQueue()));
                    Console.WriteLine(string.Concat("Order No      : ", this.getQueue()));

                    this._logger.Info(string.Concat("Copy of Print : ", COUNT_RECEIPT));
                    Console.WriteLine(string.Concat("Copy of Print : ", COUNT_RECEIPT));

                    Console.WriteLine("                Processing ...");
                    
                    int i = 1;
                    
                    for (var t=0;t < COUNT_RECEIPT; t++)
                    {
                        this._logger.Info(string.Concat("Print at : ", i));
                        Console.WriteLine(string.Concat("Print at : ", i));
                        
                        PrintDocument pd = new PrintDocument();
                        pd.PrintPage += new PrintPageEventHandler(this.ProvideContent);
                        pd.PrinterSettings.PrinterName = MASTER_PRINTER;

                        if (pd.PrinterSettings.IsValid)
                        {
                            pd.Print();
                            pd.Dispose();
                        }
                        else
                        {
                            Console.WriteLine("Printer is invalid.");
                        }
                        
                        count = 0;//reset new page by order id,do not remove!!!!

                        this._logger.Info("Printer Job : COMPLETED\n");
                        Console.WriteLine("Printer Job : COMPLETED\n");
                        Thread.Sleep(300);
                        
                        i++;

                    }
                    
                    Console.WriteLine("*******************************************************************************************");

                    try
                    {
                        (new SoundPlayer("sounds/phone1.wav")).Play();
                        Thread.Sleep(200);
                    }
                    catch (Exception y)
                    {
                        Console.WriteLine(y.Message);
                    }

                }
                else
                {
                    _logger.Info("Invalid Data Format");
                    Console.WriteLine("Invalid Data Format");
                }
                
                
            }
            catch (InvalidPrinterException invalidPrinterException1)
            {
                var LineNumber = new StackTrace(invalidPrinterException1, true).GetFrame(0).GetFileLineNumber();

                Console.WriteLine("Error at line :" + LineNumber + " ==>" + invalidPrinterException1.Message);
                /*

                InvalidPrinterException e = invalidPrinterException1;
                this._logger.Info(e.Message);
                PCPrint.PostJson("https://maker.ifttt.com/trigger/backend_alert/with/key/c4VTKqDv6MkrNGjvQISqAh", new template()
                {
                    value1 = "Printer Services at : "+MERCHANT_NAME,
                    value2 = "Receipt's Printer",
                    value3 = string.Concat("Message : ", e.Message)
                });
                */
            }
        }

        
        public void PrintReceiptFailed(JObject json, string fileX, string fileY)
        {
            this.fileReceipt = json;
            int size = (new StringBuilder(256)).Capacity;
            this.Font = new Font("Tahoma", 8f);
            PrintDocument pd = new PrintDocument();
            pd.PrintPage += new PrintPageEventHandler(this.ProvideReceiptFailed);
            pd.PrinterSettings.PrinterName = MASTER_PRINTER;
            Margins margins = new Margins(100, 100, 100, 200);
            pd.DefaultPageSettings.Margins = margins;
            pd.Print();
            pd.Dispose();
            try
            {
                (new SoundPlayer("sounds/phone1.wav")).Play();
                Thread.Sleep(200);
            }
            catch (Exception u)
            {

            }

            if (File.Exists(fileX) && !this.IsFileLocked(fileX))
            {
                File.Delete(fileX);
            }
            Thread.Sleep(200);
        }

        public void PrintQR(JObject json, string fileX, string qrFile)
        {
            this.fileReceipt = json;
            this.QRFile = qrFile;

            string printers = QR_PRINTERS;
            string[] qr_printers = printers.Split(',');

            foreach (var printer in qr_printers)
            {
                int size = (new StringBuilder(256)).Capacity;
                this.Font = new Font("Tahoma", 8f);
                PrintDocument pd = new PrintDocument();
                pd.PrintPage += new PrintPageEventHandler(this.ProvideQRCode);
                pd.PrinterSettings.PrinterName = printer;
                Margins margins = new Margins(100, 100, 100, 200);
                pd.DefaultPageSettings.Margins = margins;

                if (pd.PrinterSettings.IsValid)
                {
                    pd.Print();
                    pd.Dispose();
                }
                else
                {
                    Console.WriteLine("Printer is invalid.");
                }



                try
                {
                    (new SoundPlayer("sounds/phone1.wav")).Play();
                    Thread.Sleep(200);
                }
                catch (Exception u)
                {

                }
            }

            if (File.Exists(fileX) && !this.IsFileLocked(fileX))
            {
                File.Delete(fileX);

            }

            if (File.Exists(qrFile) && !this.IsFileLocked(qrFile))
            {
                File.Delete(qrFile);

            }

            Thread.Sleep(200);
        }

        public void PrintCallWaiter(JObject json, string fileX)
        {
            this.fileReceipt = json;
            int size = (new StringBuilder(256)).Capacity;
            this.Font = new Font("Tahoma", 8f);
            PrintDocument pd = new PrintDocument();
            pd.PrintPage += new PrintPageEventHandler(this.ProvideCallWaiter);
            pd.PrinterSettings.PrinterName = MASTER_PRINTER;
            Margins margins = new Margins(100, 100, 100, 200);
            pd.DefaultPageSettings.Margins = margins;
            pd.Print();
            pd.Dispose();
            try
            {
                (new SoundPlayer("sounds/phone1.wav")).Play();
                Thread.Sleep(200);
            }
            catch (Exception u)
            {

            }

            if (File.Exists(fileX) && !this.IsFileLocked(fileX))
            {
                File.Delete(fileX);
            }

            Thread.Sleep(200);
        }
        
        public void PrintToKitchen()
        {

            List<string>[] printer = new List<string>[] { new List<string>() };
            
            try
            {
                String[] p = DOCKET_PRINTER.Split(',');
                foreach(var a in p)
                {
                    printer[0].Add(a.ToString());
                }
                
                int count = 0; //count the loop of printer folder

                for (int bg = 0; bg < printer[0].Count<string>(); bg++)
                {

                    try
                    {

                        DirectoryInfo fKitchen = new DirectoryInfo(string.Concat("C:\\log\\printer\\KITCHEN\\", printer[0][bg]));
                        FileInfo[] files = fKitchen.GetFiles("*.json");

                        if ((int)files.Length > 0)
                        {
                            
                            for (int j = 0; j < (int)files.Length; j++)
                            {
                                FileInfo file = files[j];
                                if (!this.IsDirectoryEmpty(string.Concat("C:\\log\\printer\\KITCHEN\\", printer[0][bg])))
                                {
                                    string source = string.Concat(file.DirectoryName, "\\", file.Name);
                                    
                                    if (!File.Exists(source))
                                    {
                                        this._logger.Info(string.Concat(printer[0][bg].ToString(), "Empty"));
                                    }
                                    else
                                    {

                                        _logger.Info("Printing to Kitchen : " + source);
                                        Console.WriteLine("Printing to Kitchen : " + source);


                                        using (StreamReader r = new StreamReader(source))
                                        {
                                            this.fileReceipt = JObject.Parse(r.ReadToEnd());
                                            this._logger.Info(string.Concat("Printer Data : ", this.fileReceipt));
                                            Console.WriteLine(string.Concat("Printer Data : ", this.fileReceipt));

                                            r.Close();
                                        }

                                        try
                                        {
                                            int size = (new StringBuilder(256)).Capacity;
                                            
                                            string printerName = printer[0][bg];

                                            this._logger.Info(string.Concat("Printer Name : ", printerName));
                                            Console.WriteLine(string.Concat("Printer Name : ", printerName));

                                            this._logger.Info(string.Concat("Copy of Print : ", COUNT_DOCKET));
                                            Console.WriteLine(string.Concat("Copy of Print : ", COUNT_DOCKET));

                                            for(var t=0;t< COUNT_DOCKET; t++)
                                            {
                                                PrintDocument pd = new PrintDocument();
                                                pd.PrintPage += new PrintPageEventHandler(this.ProvideContentKitchen);
                                                pd.PrinterSettings.PrinterName = printerName;
                                                pd.Print();
                                                pd.Dispose();

                                                count = 0;//reset page 

                                            }
                                            
                                            this._logger.Info("Printer Job : COMPLETED\n");
                                            Console.WriteLine("Printer Job : COMPLETED\n");
                                            Console.WriteLine("*******************************************************************************************");

                                            try
                                            {
                                                SoundPlayer player = new SoundPlayer("sounds/phone1.wav");

                                                Thread.Sleep(200);

                                            }
                                            catch (Exception y)
                                            {


                                            }


                                            Thread.Sleep(300);

                                            if (File.Exists(source))
                                            {

                                                if (!this.IsFileLocked(source))
                                                {
                                                    try
                                                    {
                                                        File.Delete(source);
                                                        this._logger.Info(string.Concat("Printer File : ", source, " Deleted."));
                                                        Thread.Sleep(200);
                                                    }
                                                    catch (Exception exception)
                                                    {
                                                        Exception ex = exception;
                                                        this._logger.Error(string.Concat("Printer File : ", source, " Could not be deleted!!!", ex.Message.ToString()));
                                                    }
                                                }
                                            }

                                            this._logger.Info("Printer END");
                                            this._logger.Info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                        }
                                        catch (InvalidPrinterException invalidPrinterException)
                                        {

                                            InvalidPrinterException e = invalidPrinterException;
                                            this._logger.Error(string.Concat("Printer Job : failed with reason >>> ", e.Message));
                                            /*
                                            PCPrint.PostJson("https://maker.ifttt.com/trigger/backend_alert/with/key/c4VTKqDv6MkrNGjvQISqAh", new template()
                                            {
                                                value1 = "Printer Services",
                                                value2 = "Kitchen's Printer",
                                                value3 = string.Concat("Message : ", e.Message)
                                            });
                                            */
                                            return;
                                        }
                                    }
                                }

                                Thread.Sleep(200);
                            }
                        }
                    }
                    catch (InvalidPrinterException invalidPrinterException1)
                    {
                        InvalidPrinterException e = invalidPrinterException1;
                        this._logger.Info(string.Concat("Error : ", e.Message));
                        Console.WriteLine(e.Message);

                        /*
                        PCPrint.PostJson("https://maker.ifttt.com/trigger/backend_alert/with/key/c4VTKqDv6MkrNGjvQISqAh", new template()
                        {
                            value1 = "Printer Services",
                            value2 = "Kitchen's Printer",
                            value3 = string.Concat("Message : ", e.Message)
                        });
                        */
                        break;
                    }

                    count++;

                }
                

            }
            catch (Exception t)
            {
                var LineNumber = new StackTrace(t, true).GetFrame(0).GetFileLineNumber();

                Console.WriteLine("Error at line :" + LineNumber + " ==>" + t.Message + "\n");
            }

        }
        
        public string getHeaderCustomer()
        {
            string phone = "";
            string header = "";

            string merchant = this.getMerchant();
            string address = (string.IsNullOrEmpty(this.fileReceipt["merchant"]["address"].ToString()) ? "-" : this.fileReceipt["merchant"]["address"].ToString());

            if (IS_SHOW_PHONE)
            {
                phone = (string.IsNullOrEmpty(this.fileReceipt["merchant"]["phone"].ToString()) ? "-" : this.fileReceipt["merchant"]["phone"].ToString());
                
            }

            header = string.Concat(new string[] { merchant, "\n", address, "\n", phone });


            header += this.setLines(2);

            return header.ToUpper();

        }

        public string getHeaderBill()
        {

            string header = "";

            header += string.Concat("Date & Time : ", this.getDateTime());
            header += this.setLines(1);

            if (IS_SHOW_ORDER_NO)
            {
                header += string.Concat("Order No      : ", this.getQueue());
                header += this.setLines(1);

            }

            if (IS_SHOW_ORDER_TYPE)
            {
                header += string.Concat("Order Type   : ", this.getOrderType());
                header += this.setLines(1);
            }
            
            if (IS_SHOW_CUSTOMER)
            {
                header += string.Concat("Customer     : ", this.getCustomer());
                header += this.setLines(1);
            }

            
            if (IS_SHOW_TERMINAL)
            {
                header += string.Concat("Terminal      : ", this.getTerminal());
                header += this.setLines(1);
            }


            if (IS_BUZZER)
            {
                if (FIELD_TABLE)
                {
                    header += string.Concat("Table           : ", this.getTableNo());
                }
                else
                {
                    header += string.Concat("Table           : ", this.getBuzzerNo());
                }

                
                header += this.setLines(1);
            }

            header += this.getDelimiter();

            return header;

        }

        public string setLines(int line)
        {
            string ln = string.Empty;

            for (var a = 0; a < line; a++)
            {
                ln += "\n";
            }

            return ln;

        }

        public string setTabs(int tab)
        {
            string tb = string.Empty;

            for (var a = 0; a < tab; a++)
            {
                tb += "\t";
            }

            return tb;

        }

        public string getDelimiter()
        {
            return "-----------------------------------------------------------------------";
        }

        public string getBillItem()
        {
            string payment = "Grand ";
            string items = string.Empty;
            string formatMoney = "F";

            foreach (JToken a in (IEnumerable<JToken>)this.fileReceipt["orders"]["orderPayments"])
            {
                payment = a["payment_type"].ToString();
            }

            foreach (JToken a in (IEnumerable<JToken>)this.fileReceipt["orders"]["orderItemDetails"])
            {
                int qty = int.Parse(a["quantity"].ToString());
                double rate = double.Parse(a["base_price"].ToString());
                double num = rate * (double)qty;
                double itemPrice = double.Parse(num.ToString());
                string price = "";

                if (this.getCurrrency() == "Rp")
                {
                    itemPrice = Convert.ToInt32(num.ToString());
                    price = String.Format("{0:C0}", itemPrice).Trim(new Char[] { '$' });


                }
                else
                {
                    price = itemPrice.ToString(formatMoney);
                }


                string sku_name = this.SplitString(a["sku_name"].ToString());

                if (!String.IsNullOrEmpty(sku_name))
                {
                    sku_name = "(" + sku_name + ")";
                }

                string dishName = this.SplitString(a["dish_name"].ToString() + " " + sku_name);

                items += string.Concat(a["quantity"], " ", dishName);
                items += this.setTabs(1);
                items += price;

                items += this.setLines(1);


                //customization items

                foreach (JToken b in (IEnumerable<JToken>)a["orderCustomizationItemDetails"])
                {
                    foreach (JToken c in (IEnumerable<JToken>)b["orderCustomizationOptionItemDetails"])
                    {
                        double amount = double.Parse(c["amount"].ToString());
                        string dish = "";

                        try
                        {
                            if (!string.IsNullOrEmpty(c["customizationOptions"]["skus"]["name"].ToString()))
                            {
                                dish = this.SplitString(c["customization_option_name"] + " (" + c["customizationOptions"]["skus"]["name"].ToString() + ")");
                            }
                            else
                            {
                                dish = this.SplitString(c["customization_option_name"].ToString());
                            }

                        }
                        catch (Exception t)
                        {
                            dish = this.SplitString(c["customization_option_name"].ToString());
                        }

                        items += string.Concat(new object[] { "  -- ", c["quantity"], "*", dish });

                        if (amount.ToString("F") != "0.00")
                        {
                            items += string.Concat(this.getCurrrency(), " ", amount.ToString(formatMoney));
                        }

                        //for krispykreme
                        //graphics.DrawString(txtAmount, new Font("Tahoma", 9f, FontStyle.Bold), new SolidBrush(Color.Black), left, (float)(startY + Offset));
                        items += this.setLines(1);

                    }
                }
            }

            items += this.getDelimiter();

            items += this.setLines(1);


            double subtotal = double.Parse(this.fileReceipt["orders"]["order_amount"].ToString());
            double.Parse(this.fileReceipt["orders"]["order_amount"].ToString());
            double rounding = double.Parse(this.fileReceipt["orders"]["total_rounding"].ToString());
            double service_charge = double.Parse(this.fileReceipt["orders"]["total_charge"].ToString());
            double total = double.Parse(this.fileReceipt["orders"]["total_amount"].ToString());

            string subPrice, TotalPrice = "";

            if (this.getCurrrency() == "Rp")
            {
                subtotal = Convert.ToInt32(total.ToString());
                total = Convert.ToInt32(total.ToString());

                subPrice = String.Format("{0:C0}", total).Trim(new Char[] { '$' });
                TotalPrice = String.Format("{0:C0}", total).Trim(new Char[] { '$' });


            }
            else
            {
                subPrice = total.ToString(formatMoney);
                TotalPrice = total.ToString(formatMoney);
            }


            string taxes = "GST % ";
            double taxValues = 7;
            double taxFixed = 0;
            if (!string.IsNullOrEmpty(this.fileReceipt["tax"].ToString()))
            {
                string mainTax = this.fileReceipt["tax"]["taxRuleRelationList"].ToString();
                foreach (JObject ob in JArray.Parse(mainTax).Children<JObject>())
                {
                    foreach (JToken taxC in (IEnumerable<JToken>)ob["taxes"])
                    {
                        taxes = taxC["name"].ToString();
                        taxValues = double.Parse(taxC["value"].ToString());
                        taxFixed = taxValues / 107 * subtotal;
                    }
                }
            }

            string Subtotal = string.Concat(this.getCurrrency(), " ", total.ToString(formatMoney));
            string GST = string.Concat(this.getCurrrency(), " ", taxFixed.ToString(formatMoney));
            string round = string.Concat(this.getCurrrency(), " ", rounding.ToString(formatMoney));
            string service = string.Concat(this.getCurrrency(), " ", service_charge.ToString(formatMoney));
            string Grandtotal = string.Concat(this.getCurrrency(), " ", subtotal.ToString(formatMoney));

            items += "Subtotal";
            items += this.setTabs(3);
            items += subPrice;

            items += this.setLines(1);

            /*
            if (taxFixed.ToString(formatMoney) != "0.00")
            {

                graphics.DrawString(taxes, new Font("Tahoma", 9f), new SolidBrush(Color.Black), 55f, (float)(startY + Offset));
                graphics.DrawString(GST, new Font("Tahoma", 9f), new SolidBrush(Color.Black), left, (float)(startY + Offset));
                Offset += 20;

            }

            if (service_charge.ToString(formatMoney) != "0.00")
            {

                graphics.DrawString("Service Charge 10%", new Font("Tahoma", 9f), new SolidBrush(Color.Black), 55f, (float)(startY + Offset));
                graphics.DrawString(service, new Font("Tahoma", 9f), new SolidBrush(Color.Black), left, (float)(startY + Offset));
                Offset += 20;

            }

            if (rounding.ToString(formatMoney) != "0.00")
            {

                graphics.DrawString("Rounding", new Font("Tahoma", 9f), new SolidBrush(Color.Black), 55f, (float)(startY + Offset));
                graphics.DrawString(round, new Font("Tahoma", 9f), new SolidBrush(Color.Black), left, (float)(startY + Offset));
                Offset += 20;

            }
            */
            items += orderType + " Total";
            items += this.setTabs(3);
            items += subPrice;
            items += this.setLines(1);

            items += this.getDelimiter();


            if (!String.IsNullOrEmpty(payment))
            {

                items += payment;
                items += this.setTabs(2);
                items += subPrice;

                items += this.setLines(1);

            }

            return items;

        }
        
        public void ProvideContent(object sender, PrintPageEventArgs e)
        {
            try
            {
                Graphics graphics = e.Graphics;
                var items = new List<string>();
                string payment = "";
                float currentY = 0;// declare  one  variable for height measurment
                int current = Convert.ToInt32(START_Y_CONTENT_RECEIPT);
                int startX = 0;
                int startY = 0;
                int Offset = 20;
                float x = 45f;
                float y = 20f;
                float width = 210f;
                float height = 200f;
                RectangleF drawRect = new RectangleF(x, y, width, height);
                
                //string dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string dt = DateTime.Now.ToString();
                StringFormat sf = new StringFormat()
                {
                    LineAlignment = StringAlignment.Center,
                    Alignment = StringAlignment.Center
                };

                StringFormat rightAlignment = new StringFormat()
                {
                    Alignment = StringAlignment.Far
                };


                this.queue = this.getQueue();
                this.orderType = this.getOrderType();
                
                if (count == 0)
                {
                    graphics.DrawString(this.getHeaderCustomer(), DefaultFont, Brushes.Black, drawRect, sf);
                    Offset += 200;

                    if (IS_SHOW_BARCODE)
                    {
                        // generate barcode of Queue with zen barcode
                        graphics.DrawImage(this.getBarcode(this.queue), (float)startX + 80, (float)(startY + Offset));
                        Offset += 80;
                    }
                   
                    graphics.DrawString(this.getHeaderBill(), DefaultFont, Brushes.Black, (float)startX, (float)(startY + Offset));
                    Offset += 100;
                    currentY += current;

                }

                //GET PAYMENT METHOD
                foreach (JToken a in (IEnumerable<JToken>)this.fileReceipt["orders"]["orderPayments"])
                {
                    payment = a["payment_type"].ToString();
                }

                foreach (JToken a in (IEnumerable<JToken>)this.fileReceipt["orders"]["orderItemDetails"])
                {
                    int qty = int.Parse(a["quantity"].ToString());
                    int mainQty = int.Parse(a["quantity"].ToString());
                    double rate = double.Parse(a["base_price"].ToString());
                    double num = rate * (double)qty;
                    double itemPrice = double.Parse(num.ToString());
                    string price = "";

                    if (!IS_SHOW_CENT)//IDR
                    {
                        price = num.ToString("n", nfi);

                    }
                    else
                    {
                        price = num.ToString("n2");
                    }


                    string sku_name = a["sku_name"].ToString();

                    //string mainItem = a["dish_name"].ToString() + " " + sku_name + "\n";
                    string mainItem = a["dish_name"].ToString() + "\n";

                    string cekMainItem = string.Concat(a["quantity"], " ", mainItem);

                    byte[] unicodebytesMainItem = Encoding.GetEncoding("iso-8859-1").GetBytes(cekMainItem);
                    string output1 = Encoding.ASCII.GetString(unicodebytesMainItem);

                    mainItem = string.Concat(cekMainItem.Length > 30 ? this.SplitString(output1) : output1, "|", price);
                    
                    items.Add(mainItem);

                    foreach (JToken b in (IEnumerable<JToken>)a["orderCustomizationItemDetails"])
                    {
                        foreach (JToken c in (IEnumerable<JToken>)b["orderCustomizationOptionItemDetails"])
                        {
                            double amount = Double.Parse(c["amount"].ToString());
                            string amt = "";
                            amt = amount.ToString("n", nfi);

                            if (IS_SHOW_CENT)
                            {
                                amt = amount.ToString("n2");
                            }
                            string dish = "";

                            try
                            {
                                /*
                                if (!string.IsNullOrEmpty(c["customizationOptions"]["skus"]["name"].ToString()))
                                {
                                    dish = c["customization_option_name"] + " " + c["customizationOptions"]["skus"]["name"].ToString();
                                }
                                else
                                {
                                    dish = c["customization_option_name"].ToString();
                                }
                                */
                                dish = c["customization_option_name"].ToString();

                            }
                            catch (Exception t)
                            {
                                dish = c["customization_option_name"].ToString();
                            }


                            byte[] unicodebytes = Encoding.GetEncoding("iso-8859-1").GetBytes(dish);//CrÃ¨me //Crème

                            string output = Encoding.Unicode.GetString(unicodebytes);

                            string output2 = Encoding.ASCII.GetString(unicodebytes);

                            string output3 = Encoding.UTF8.GetString(unicodebytes);

                            string output4 = Encoding.Default.GetString(unicodebytes);

                            string output5 = Encoding.BigEndianUnicode.GetString(unicodebytes);

                            string output6 = Encoding.BigEndianUnicode.GetString(unicodebytes);

                            string cekStr = string.Concat(new object[] { "-- ", mainQty * int.Parse(c["quantity"].ToString()), "*", output2 });

                            items.Add(string.Concat(new object[] { cekStr.Length > 30 ? this.SplitString(cekStr) : cekStr, "|", amt }));

                        }
                    }


                }

                items.Add(this.setLines(1) + "|0");

                items.Add(this.getDelimiter() + "|0");

                //SETTING SUMMARY OF RECEIPT
                double subtotal = double.Parse(this.fileReceipt["orders"]["total_amount"].ToString());
                double total_tax = double.Parse(this.fileReceipt["orders"]["total_tax"].ToString());
                double grandtotal = double.Parse(this.fileReceipt["orders"]["order_amount"].ToString());
                double rounding = double.Parse(this.fileReceipt["orders"]["total_rounding"].ToString());
                double service_charge = double.Parse(this.fileReceipt["orders"]["total_charge"].ToString());

                string subPrice,tax,service,totRounding,totalPrice = "";

                subPrice = subtotal.ToString("n", nfi);
                tax = total_tax.ToString("n", nfi);
                service = service_charge.ToString("n", nfi);
                totRounding = rounding.ToString("n", nfi);
                totalPrice = grandtotal.ToString("n", nfi);

                if (IS_SHOW_CENT)
                {
                    subPrice = subtotal.ToString("n2", nfi);
                    subPrice = double.Parse(subPrice).ToString("n2");
                }

                items.Add("Subtotal|" + subPrice);

                //show tax
                if (IS_SHOW_TAX)
                {
                    tax = total_tax.ToString("n2", nfi);
                    tax = double.Parse(tax).ToString("n2");

                    if(total_tax > 0)
                    {
                        items.Add("Tax|" + tax);
                    }
                    

                }

                //show service charge
                if (IS_SHOW_SERVICE_CHARGE)
                {
                    service = service_charge.ToString("n2", nfi);
                    service = double.Parse(service).ToString("n2");

                    if(service_charge > 0)
                    {
                        items.Add("Service Charge|" + service);
                    }

                }


                //show rounding

                if (IS_SHOW_ROUNDING)
                {
                    totRounding = double.Parse(totRounding).ToString("n2");
                    if (rounding > 0)
                    {
                        items.Add("Rounding|" + totRounding);
                    }
                    
                }
                
                //show gst
                if (IS_SHOW_GST)
                {
                    
                    string taxes = "GST % ";
                    double taxValues = 7;
                    double taxAmount = 0;
                    double taxFixed = 0;
                    if (!string.IsNullOrEmpty(this.fileReceipt["tax"].ToString()))
                    {
                        string mainTax = this.fileReceipt["tax"]["taxRuleRelationList"].ToString();
                        foreach (JObject ob in JArray.Parse(mainTax).Children<JObject>())
                        {
                            foreach (JToken taxC in (IEnumerable<JToken>)ob["taxes"])
                            {
                                taxes = taxC["name"].ToString();
                                taxValues = double.Parse(taxC["value"].ToString());
                                taxAmount = double.Parse(taxC["amountValue"].ToString());

                                taxFixed = taxValues / 107 * subtotal;

                                if(taxValues > 0)
                                {
                                    items.Add(taxes + "| " + taxAmount.ToString("n2"));

                                }

                            }
                        }
                    }
                    
                }


                if (IS_SHOW_CENT)
                {
                    totalPrice = grandtotal.ToString("n2", nfi);
                    totalPrice = double.Parse(totalPrice).ToString("n2");
                }

                items.Add(orderType + " Total|" + totalPrice);

                items.Add(this.getDelimiter() + "|0");


                if (!String.IsNullOrEmpty(payment))
                {
                    if (!IS_TOTAL_AS_TEXT)
                    {
                        items.Add(payment + "|" + totalPrice);
                    }
                    else
                    {
                        items.Add("Total|" + totalPrice);
                    }

                }

                if (IS_SHOW_THANKS)
                {
                    items.Add(this.getDelimiter() + "|0");
                    items.Add("thanks|0");
                }

                items.Add("end|0");
                var itemx = items;

                //return;

                //END STRING COLLECTION TO BE PRINTED

                //start print items .consist of : items and summary receipt

                while (count < items.Count()) // check the number of items
                {

                    string item = items[count].ToString();
                    string[] w = item.Split('|');
                    string itm = w[0].ToString();
                    string prc = w[1].ToString();

                    if (prc.ToString() == "0" || prc.ToString() == "0.00")
                    {
                        prc = "";
                    }
                    else
                    {
                        prc = this.getCurrrency() + prc;
                    }
                    
                    try
                    {
                        if (itm != "end")
                        {
                            RectangleF drawRect1 = new RectangleF((float)Convert.ToInt32((START_X_POST_PRICE)), currentY, width, height);

                            if (itm != "thanks")
                            {
                                e.Graphics.DrawString(itm, DefaultFont, Brushes.Black, 5, currentY);
                                e.Graphics.DrawString(prc, DefaultFont, Brushes.Black, drawRect1, rightAlignment);
                            }
                            else
                            {
                                e.Graphics.DrawString(" -- Thank You --", DefaultFont, Brushes.Black, 25, currentY);
                            }
                            

                            if (itm.Contains("\r"))
                            {
                                currentY += 20;
                            }
                        }

                        currentY += 20; // set a gap between every item
                        count += 1;

                        if (itm == "end")
                        {
                            e.HasMorePages = false;
                            count = 0;
                            break;
                        }


                        if (ix < 30) // if  the number of item(per page) is less than 50 
                        {
                            ix += 1; // increment i by +1
                            e.HasMorePages = false; // set the HasMorePages property to false , so that no other page will not be added

                        }
                        else // if the number of item(per page) is more than 50
                        {

                            ix = 0;           // initiate i to 0 .
                            e.HasMorePages = true;
                            return;
                        }

                    }
                    catch (Exception d)
                    {
                        count = 0;
                        var LineNumber = new StackTrace(d, true).GetFrame(0).GetFileLineNumber();

                        Console.WriteLine("Error at line :" + LineNumber + " ==>" + d.Message);
                    }
                    
                }

            }
            catch (Exception u)
            {
                var LineNumber = new StackTrace(u, true).GetFrame(0).GetFileLineNumber();

                Console.WriteLine("Error at line :" + LineNumber + " ==>" + u.Message);
                
            }

        }
        
        public void ProvideContentKitchen(object sender, PrintPageEventArgs e)
        {

            Graphics graphics = e.Graphics;
            var items = new List<string>();

            float currentY = 0;// declare  one  variable for height measurment
            int current = Convert.ToInt32(START_Y_CONTENT_DOCKET);
            int startX = 0;
            int startY = Convert.ToInt32(PADDING_TOP_DOCKET);
            int Offset = 20;
            float x = 45f;
            float y = 10f;
            
            try
            {
                string header = (string.IsNullOrEmpty(this.fileReceipt["Header"].ToString()) ? "-" : this.fileReceipt["Header"].ToString());
                queue = (string.IsNullOrEmpty(this.fileReceipt["OrderNo"].ToString()) ? "-" : this.fileReceipt["OrderNo"].ToString());
                string content = "";
                
                int n = 0;
                
                if (this.fileReceipt["Content"].Count<JToken>() > 0)
                {
                    foreach (JToken itm in (IEnumerable<JToken>)this.fileReceipt["Content"])
                    {
                        if (n > 0)
                        {
                            content = string.Concat(content, "\n", itm);
                            items.Add(itm.ToString());
                        }
                        n++;
                    }
                }

                items.Add("end|0");
                

                //start print
                if (count == 0)
                {
                   
                    graphics.DrawString(this.queue, new Font("Tahoma", (float)FONT_DOCKET_ORDER_SIZE, FontStyle.Bold), new SolidBrush(Color.Black), (float)startX, (float)startY);
                    Offset += 30;
                    graphics.DrawString(header, new Font("Tahoma", 10f, FontStyle.Bold), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
                    Offset += 20;
                    currentY += current;
                }
                   
                //return;

                while (count < items.Count()) // check the number of items
                {

                    string item = items[count].ToString();
                    string[] w = item.Split('|');
                    string itm = w[0].ToString();
                   
                    try
                    {
                        if (itm != "end")
                        {
                            
                            if (itm.Contains("      "))//if item customization
                            {
                                e.Graphics.DrawString(itm, new Font("Tahoma", (float)FONT_DOCKET_CONTENT_SIZE, FontStyle.Regular), new SolidBrush(Color.Black), 5, currentY);
                            }
                            else
                            {
                                e.Graphics.DrawString(itm, new Font("Tahoma", (float)FONT_DOCKET_CONTENT_SIZE, FontStyle.Bold), new SolidBrush(Color.Black), 5, currentY);
                            }


                            if (itm.Contains("\r"))
                            {
                                currentY += 20;
                            }
                        }

                        currentY += 20; // set a gap between every item
                        count += 1;

                        if (itm == "end")
                        {
                            e.HasMorePages = false;
                            count = 0;
                            break;
                        }


                        if (ix < 30) // if  the number of item(per page) is less than 50 
                        {
                            ix += 1; // increment i by +1
                            e.HasMorePages = false; // set the HasMorePages property to false , so that no other page will not be added

                        }
                        else // if the number of item(per page) is more than 50
                        {

                            ix = 0;           // initiate i to 0 .
                            e.HasMorePages = true;
                            return;
                        }

                    }
                    catch (Exception d)
                    {
                        count = 0;
                        var LineNumber = new StackTrace(d, true).GetFrame(0).GetFileLineNumber();

                        Console.WriteLine("Error at line :" + LineNumber + " ==>" + d.Message);
                    }

                }

            }
            catch (Exception r)
            {
                var LineNumber = new StackTrace(r, true).GetFrame(0).GetFileLineNumber();

                Console.WriteLine("Error at line :" + LineNumber + " ==>" + r.Message + "\n");

            }
            
        }

        public void ProvideReceiptFailed(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Tahoma", 10f);
            string dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            StringFormat sf = new StringFormat()
            {
                LineAlignment = StringAlignment.Center,
                Alignment = StringAlignment.Center
            };
            font.GetHeight();
            int startX = 0;
            int startY = 0;
            int Offset = 20;
            string merchant = (string.IsNullOrEmpty(this.fileReceipt["merchant"]["name"].ToString()) ? "-" : this.fileReceipt["merchant"]["name"].ToString());
            string address = (string.IsNullOrEmpty(this.fileReceipt["merchant"]["address"].ToString()) ? "-" : this.fileReceipt["merchant"]["address"].ToString());
            string phone = (string.IsNullOrEmpty(this.fileReceipt["merchant"]["phone"].ToString()) ? "-" : this.fileReceipt["merchant"]["phone"].ToString());
            string queue = (string.IsNullOrEmpty(this.fileReceipt["orders"]["queue_no"].ToString()) ? "-" : this.fileReceipt["orders"]["queue_no"].ToString());
            string orderType = (string.IsNullOrEmpty(this.fileReceipt["orders"]["orderTypes"]["name"].ToString()) ? "-" : this.fileReceipt["orders"]["orderTypes"]["name"].ToString());
            string orderTable = (string.IsNullOrEmpty(this.fileReceipt["orders"]["buzzer_no"].ToString()) ? "-" : this.fileReceipt["orders"]["buzzer_no"].ToString());
            string terminal = (string.IsNullOrEmpty(this.fileReceipt["orders"]["terminal"].ToString()) ? "-" : this.fileReceipt["orders"]["terminal"].ToString());
            string currency = this.fileReceipt["merchant"]["currency_type"].ToString();
            float x = 45f;
            float y = 10f;
            float width = 210f;
            float height = 100f;
            RectangleF drawRect = new RectangleF(x, y, width, height);
            RectangleF drawRect1 = new RectangleF(x, 90f, width, height);
            StringFormat drawFormat = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };
            string error = this.fileReceipt["orders"]["logOrders"]["type"].ToString();
            string response = this.fileReceipt["orders"]["logOrders"]["respone"].ToString();
            JObject jo = JObject.Parse(response);
            string message = jo["message"].ToString();
            string header = string.Concat(new string[] { merchant, "\n", address, "\n", phone });
            int xy = 100;
            if (error != "1")
            {
                graphics.DrawString(header, new Font("Tahoma", 9f), new SolidBrush(Color.Black), drawRect, drawFormat);
            }
            else
            {
                xy = 140;
                string underLine1 = "*****************************";
                graphics.DrawString(string.Concat(new string[] { underLine1, "\n", message, "\n", underLine1 }), new Font("Tahoma", 8f), new SolidBrush(Color.Black), drawRect, drawFormat);
                Offset += 40;
                graphics.DrawString(header, new Font("Tahoma", 9f), new SolidBrush(Color.Black), drawRect1, drawFormat);

            }
            Offset += xy;
            graphics.DrawString(string.Concat("               Order Number : ", queue), new Font("Tahoma", 12f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 20;
            string underLine = "----------------------------------------------------";
            graphics.DrawString(underLine, new Font("Tahoma", 12f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 20;
            graphics.DrawString(string.Concat("Order Type  : ", orderType), new Font("Tahoma", 9f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 20;
            graphics.DrawString(string.Concat("Date & Time: ", dt), new Font("Tahoma", 9f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 20;
            graphics.DrawString(string.Concat("Terminal: ", terminal), new Font("Tahoma", 9f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 20;
            graphics.DrawString(string.Concat("Table: ", orderTable), new Font("Tahoma", 9f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 20;
            graphics.DrawString(underLine, new Font("Tahoma", 12f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 20;
            foreach (JToken a in (IEnumerable<JToken>)this.fileReceipt["orders"]["orderItemDetails"])
            {
                int qty = int.Parse(a["quantity"].ToString());
                double rate = double.Parse(a["base_price"].ToString());
                double num = rate * (double)qty;
                double itemPrice = double.Parse(num.ToString());
                graphics.DrawString(string.Concat(a["quantity"], " ", a["dish_name"]), new Font("Tahoma", 9f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
                graphics.DrawString(string.Concat(currency, " ", itemPrice.ToString("F")), new Font("Tahoma", 9f), new SolidBrush(Color.Black), 235f, (float)(startY + Offset));
                Offset += 20;
                foreach (JToken b in (IEnumerable<JToken>)a["orderCustomizationItemDetails"])
                {
                    foreach (JToken c in (IEnumerable<JToken>)b["orderCustomizationOptionItemDetails"])
                    {
                        double amount = double.Parse(c["amount"].ToString());
                        string txtAmount = "";
                        graphics.DrawString(string.Concat(new object[] { "  -- ", c["quantity"], " ", c["customization_option_name"] }), new Font("Tahoma", 9f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
                        if (amount.ToString("F") != "0.00")
                        {
                            txtAmount = string.Concat(currency, " ", amount.ToString("F"));
                        }
                        graphics.DrawString(txtAmount, new Font("Tahoma", 9f), new SolidBrush(Color.Black), 235f, (float)(startY + Offset));
                        Offset += 20;
                    }
                }
            }
            graphics.DrawString(underLine, new Font("Tahoma", 12f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 20;
            double subtotal = double.Parse(this.fileReceipt["orders"]["order_amount"].ToString());
            double.Parse(this.fileReceipt["orders"]["order_amount"].ToString());
            double rounding = double.Parse(this.fileReceipt["orders"]["total_rounding"].ToString());
            double service_charge = double.Parse(this.fileReceipt["orders"]["total_charge"].ToString());
            double total = double.Parse(this.fileReceipt["orders"]["total_amount"].ToString());

            string taxes = "GST % ";
            double taxValues = 7;
            double taxFixed = 0;
            if (!string.IsNullOrEmpty(this.fileReceipt["tax"].ToString()))
            {
                string mainTax = this.fileReceipt["tax"]["taxRuleRelationList"].ToString();
                foreach (JObject ob in JArray.Parse(mainTax).Children<JObject>())
                {
                    foreach (JToken taxC in (IEnumerable<JToken>)ob["taxes"])
                    {
                        taxes = taxC["name"].ToString();
                        taxValues = double.Parse(taxC["value"].ToString());
                        taxFixed = taxValues / 107 * subtotal;
                    }
                }
            }
            string Subtotal = string.Concat(currency, " ", total.ToString("F"));
            string GST = string.Concat(currency, " ", taxFixed.ToString("F"));
            string round = string.Concat(currency, " ", rounding.ToString("F"));
            string service = string.Concat(currency, " ", service_charge.ToString("F"));
            string Grandtotal = string.Concat(currency, " ", subtotal.ToString("F"));

            graphics.DrawString("Subtotal", new Font("Tahoma", 9f), new SolidBrush(Color.Black), 100f, (float)(startY + Offset));
            graphics.DrawString(Subtotal, new Font("Tahoma", 9f), new SolidBrush(Color.Black), 237f, (float)(startY + Offset));
            Offset += 20;

            if (taxFixed.ToString("F") != "0.00")
            {
                graphics.DrawString(taxes, new Font("Tahoma", 9f), new SolidBrush(Color.Black), 100f, (float)(startY + Offset));
                graphics.DrawString(GST, new Font("Tahoma", 9f), new SolidBrush(Color.Black), 237f, (float)(startY + Offset));
                Offset += 20;
            }

            if (service_charge.ToString("F") != "0.00")
            {
                graphics.DrawString("Service Charge 10%", new Font("Tahoma", 9f), new SolidBrush(Color.Black), 100f, (float)(startY + Offset));
                graphics.DrawString(service, new Font("Tahoma", 9f), new SolidBrush(Color.Black), 237f, (float)(startY + Offset));
                Offset += 20;
            }

            if (rounding.ToString("F") != "0.00")
            {
                graphics.DrawString("Rounding", new Font("Tahoma", 9f), new SolidBrush(Color.Black), 100f, (float)(startY + Offset));
                graphics.DrawString(round, new Font("Tahoma", 9f), new SolidBrush(Color.Black), 237f, (float)(startY + Offset));
                Offset += 20;
            }

            graphics.DrawString("Grandtotal", new Font("Tahoma", 9f), new SolidBrush(Color.Black), 100f, (float)(startY + Offset));
            graphics.DrawString(Grandtotal, new Font("Tahoma", 9f), new SolidBrush(Color.Black), 237f, (float)(startY + Offset));
            Offset += 20;
            graphics.DrawString(underLine, new Font("Tahoma", 12f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 20;

            graphics.DrawString("      Please Proceed To The Cashier --Thank You", new Font("Tahoma", 9f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 20;

        }

        public void ProvideCallWaiter(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Tahoma", 10f);
            string dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            StringFormat sf = new StringFormat()
            {
                LineAlignment = StringAlignment.Center,
                Alignment = StringAlignment.Center
            };
            font.GetHeight();
            int startX = 0;
            int startY = 0;
            int Offset = -5;
            float x = 45f;
            float y = 1f;
            float width = 210f;
            float height = 100f;
            string underLine = "----------------------------------------------------";
            RectangleF drawRect = new RectangleF(x, y, width, height);
            RectangleF drawRect1 = new RectangleF(x, 40f, width, height);
            StringFormat drawFormat = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            graphics.DrawString(underLine, new Font("Tahoma", 12f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 80;
            graphics.DrawString("Room   : " + this.fileReceipt["terminal"].ToString() + "\n\n" + this.fileReceipt["freetext"].ToString() + "\n\n", new Font("Tahoma", 12f, FontStyle.Bold), new SolidBrush(Color.Black), drawRect, drawFormat);
            graphics.DrawString(underLine, new Font("Tahoma", 12f), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));
            Offset += 20;
        }

        public void ProvideQRCode(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Tahoma", 10f);
            string dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            StringFormat sf = new StringFormat()
            {
                LineAlignment = StringAlignment.Center,
                Alignment = StringAlignment.Center
            };
            font.GetHeight();
            int startX = 5;
            int startY = 0;
            int Offset = 0;
            float x = 0f; //bixoleon 25 epson 25
            float y = 20f;
            float width = 250f;
            float height = 450;
            string underLine = "----------------------------------------------------";
            RectangleF drawRect = new RectangleF(x, y, width, height);
            RectangleF drawRect1 = new RectangleF(x, 120f, width, height);

            StringFormat drawFormat = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            graphics.DrawString("Table No : " + this.fileReceipt["tableNo"].ToString(), new Font("Tahoma", 16f, FontStyle.Bold), new SolidBrush(Color.Black), 50, startY + Offset); //bixolon 50 epson 70

            Image img = System.Drawing.Image.FromFile(this.QRFile);
            e.Graphics.DrawImage(img, startX, 25 + Offset); //bixolon 25 epson 30
            img.Dispose();

            Offset += 245;
            graphics.DrawString("\n" + this.fileReceipt["instruction"].ToString(), new Font("Tahoma", 9f, FontStyle.Bold), new SolidBrush(Color.Black), drawRect1, drawFormat);
            
            //graphics.DrawString("Table No : " + this.fileReceipt["tableNo"].ToString() + "\n" + this.fileReceipt["instruction"].ToString(), new Font("Tahoma", 9f, FontStyle.Bold), new SolidBrush(Color.Black), (float)startX, (float)(startY + Offset));

            Offset += 120;
        }

        public int RemoveZeros(int value)
        {
            return (value == 0 ? 1 : value);
        }
        

        public String getDateTime()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public String getMerchant()
        {
            String m = String.Empty;
            try
            {
                m= (string.IsNullOrEmpty(this.fileReceipt["merchant"]["name"].ToString()) ? "-" : this.fileReceipt["merchant"]["name"].ToString());
            }
            catch(Exception g)
            {

            }
            return m;
        }

        public String getCustomer()
        {
            String u = string.Empty;
            try
            {
                u= (string.IsNullOrEmpty(this.fileReceipt["orders"]["customer_name"].ToString()) ? "-" : this.fileReceipt["orders"]["customer_name"].ToString());
            }
            catch(Exception g)
            {

            }

            return u;
        }

        public String getAddress()
        {
            String a = String.Empty;
            try
            {
                a=(string.IsNullOrEmpty(this.fileReceipt["merchant"]["address"].ToString()) ? "-" : this.fileReceipt["merchant"]["address"].ToString());
            }
            catch(Exception g)
            {

            }
            return a;
        }

        public String getPhone()
        {
            String p = String.Empty;
            try
            {
                p = (string.IsNullOrEmpty(this.fileReceipt["merchant"]["phone"].ToString()) ? "-" : this.fileReceipt["merchant"]["phone"].ToString());
            }
            catch (Exception g)
            {

            }
            return p;
            
        }

        public String getQueue()
        {
            String q = String.Empty;
            try
            {
                if (this.fileReceipt.ContainsKey("orders"))
                {
                    q = (string.IsNullOrEmpty(this.fileReceipt["orders"]["queue_no"].ToString()) ? "-" : this.fileReceipt["orders"]["queue_no"].ToString());
                    queue = q;
                }

            }
            catch (Exception g)
            {
                
            }
            return q;

        }

        public String getBuzzerNo()
        {
            String b = String.Empty;
            try
            {
                
                b = (string.IsNullOrEmpty(this.fileReceipt["orders"]["buzzer_no"].ToString()) ? "-" : this.fileReceipt["orders"]["buzzer_no"].ToString());
            }
            catch (Exception g)
            {

            }
            return b;
            
        }

        public String getTerminal()
        {
            String t = String.Empty;
            try
            {
                t = (string.IsNullOrEmpty(this.fileReceipt["orders"]["terminal"].ToString()) ? "-" : this.fileReceipt["orders"]["terminal"].ToString());
            }
            catch (Exception g)
            {

            }
            return t;
            
        }

        public String getTableNo()
        {
            String t = String.Empty;
            try
            {
                t = (string.IsNullOrEmpty(this.fileReceipt["orders"]["orderPosInfos"]["table_no"].ToString()) ? "-" : this.fileReceipt["orders"]["orderPosInfos"]["table_no"].ToString());
            }
            catch (Exception g)
            {

            }
            return t;
        }

        public String getOrderType()
        {
            String t = String.Empty;
            try
            {
                t = (string.IsNullOrEmpty(this.fileReceipt["orders"]["orderTypes"]["name"].ToString()) ? "-" : this.fileReceipt["orders"]["orderTypes"]["name"].ToString().ToUpper());

            }
            catch (Exception g)
            {

            }
            return t;
        }

        public String getCurrrency()
        {
            String t = String.Empty;
            try
            {
                t = (string.IsNullOrEmpty(this.fileReceipt["merchant"]["currency_type"].ToString()) ? "$" : this.fileReceipt["merchant"]["currency_type"].ToString());

            }
            catch (Exception g)
            {

            }
            return t;

        }

        public string getHeaderDoket()
        {
            var header = string.Concat("Order type          : ", this.getOrderType(), "\n");
            header = string.Concat(header, "Date & Time       : ", this.getDateTime(), "\n");

            if (IS_BUZZER)
            {
                if (FIELD_TABLE)
                {
                    header = string.Concat(header, "Terminal / Table : ", this.getTerminal() + " - " + this.getTableNo(), "\n");
                }
                else
                {
                    header = string.Concat(header, "Terminal / Table : ", this.getTerminal() + " - " + this.getBuzzerNo(), "\n");
                }
                
            }
            else
            {
                header = string.Concat(header, "Terminal             : ", this.getTerminal() , "\n");
            }

            if (IS_BUZZER)
            {
                if (FIELD_TABLE)
                {
                    header = string.Concat(header, "Table No             : ", this.getTableNo(), "\n");
                }
                else
                {
                    header = string.Concat(header, "Table No             : ", this.getBuzzerNo(), "\n");
                }
                
            }

            if (IS_SHOW_PRINTER)
            {
                header = string.Concat(header, "Printer                 : ", this.getPrinterName(), "\n");
            }

            header = string.Concat(header, this.getDelimiter(), "\n");


            return header;
        }

        public String getPrinterName()
        {
            return this.printerName;
        }

        public  String dataCollect(JObject json)
        {
            this.fileReceipt = json;
            String response = String.Empty;

            response = "\rMerchant : " + this.getMerchant();
            response += "\rOrder Type : " + this.getOrderType();
            response += "\rTerminal : " + this.getTerminal();
            response += "\rOrder No : " + this.getQueue();
            response += "\rTable No : " + this.getBuzzerNo();
            response += "\rData File : " + json.ToString();

            _logger.Info("Merchant: " + this.getMerchant());
            _logger.Info("Order Type : " + this.getOrderType());
            _logger.Info("Terminal : " + this.getTerminal());
            _logger.Info("Order No : " + this.getQueue());
            _logger.Info("Table No : " + this.getBuzzerNo());
            _logger.Info("Source Data File : " + json.ToString(Newtonsoft.Json.Formatting.None));

            if (IS_SEND_IFTTT)
            {
                try
                {
                    PostJson("https://maker.ifttt.com/trigger/backend_alert/with/key/c4VTKqDv6MkrNGjvQISqAh", new template()
                    {
                        value1 = "Standalone : " + this.getMerchant(),
                        value2 = "Order No : " + this.getQueue(),
                        value3 = json.ToString(Newtonsoft.Json.Formatting.None)
                    });
                    _logger.Info("Send to IFTTT : Success");
                    Console.WriteLine("Send to IFTTT : Success");

                }
                catch (Exception u)
                {
                    _logger.Info("Send to IFTTT : Failed");
                    Console.WriteLine("Send to IFTTT : Failed");
                }

                
            }
            
            return response;
            
        }
        public void SplitReceipt(JObject json)
        {
            Constant cons = new Constant();
            try
            {
                _logger.Info("Pointing Data to KP ...");
                Console.WriteLine("Pointing Data to KP ...");

                if (this.fileReceipt.ContainsKey("orders")){
                    this.fileReceipt = json;
                    List<string>[] kp = new List<string>[1];
                    string name_print = "";
                    int n = 0;
                    int count_printers = DOCKET_PRINTER.Split(',').Count();

                    kp = new List<string>[count_printers];

                    if (count_printers > 0)
                    {
                        foreach (var a in DOCKET_PRINTER.Split(','))
                        {
                            kp[n] = new List<string>();
                            kp[n].Add(a.ToString());
                            n++;
                        }
                    }

                    int mainQty = 1;

                    foreach (JToken a in (IEnumerable<JToken>)this.fileReceipt["orders"]["orderItemDetails"])
                    {
                        mainQty = int.Parse(a["quantity"].ToString());
                        string sku_name = this.SplitString(a["sku_name"].ToString());
                        
                        try
                        {

                            foreach (JToken b in (IEnumerable<JToken>)a["printers"])
                            {
                                for (int qx = 0; qx < count_printers; qx++)
                                {
                                    this.printerName = b["name"].ToString();//as label on header


                                    if (b["name"].ToString() == kp[qx][0])
                                    {

                                        var namePrint = String.Empty;

                                        try
                                        {
                                            namePrint = a["dish_name"].ToString();

                                        }
                                        catch (Exception i)
                                        {

                                        }


                                        //name_print = (!string.IsNullOrEmpty(namePrint) ? namePrint : a["dish_name"].ToString() + sku_name);

                                        name_print = (!string.IsNullOrEmpty(namePrint) ? namePrint : a["dish_name"].ToString());

                                        if (name_print == "NONE")
                                        {
                                            //name_print = a["dish_name"].ToString() + sku_name;
                                            name_print = a["dish_name"].ToString();
                                        }

                                        if ((name_print.Count<char>(new Func<char, bool>(char.IsWhiteSpace)) <= 3 ? false : name_print.Length > 30))
                                        {
                                            name_print = this.SplitString(name_print);
                                        }


                                        kp[qx].Add(string.Concat(mainQty.ToString(), " x ", name_print));

                                        string request = a["special_instruction"].ToString();

                                        if (!string.IsNullOrEmpty(request))
                                        {
                                            if ((request.Count<char>(new Func<char, bool>(char.IsWhiteSpace)) <= 3 ? false : request.Length > 30))
                                            {
                                                request = this.SplitString(request);
                                            }
                                            kp[qx].Add(string.Concat("** Special Request : \n      ", request));

                                            kp[qx].Add(setLines(1));
                                        }

                                        if (a["orderCustomizationItemDetails"].Count<JToken>() > 0)
                                        {
                                            foreach (JToken bx in (IEnumerable<JToken>)a["orderCustomizationItemDetails"])
                                            {
                                                if (bx["orderCustomizationOptionItemDetails"].Count<JToken>() <= 0)
                                                {
                                                    continue;
                                                }

                                                //check if customization option send as item
                                                foreach (JToken cx in (IEnumerable<JToken>)bx["orderCustomizationOptionItemDetails"])
                                                {

                                                    if (cx["customizationOptions"]["send_as_item"].ToString() != "true")
                                                    {
                                                        continue;
                                                    }

                                                    if (cx["customizationOptions"].Contains("skus"))
                                                    {
                                                        if (!cx["customizationOptions"]["skus"].Contains<JToken>("name_print"))
                                                        {
                                                            //name_print = cx["customizationOptions"]["skus"]["name"].ToString();
                                                            name_print = cx["customizationOptions"]["name"].ToString();
                                                        }
                                                        else
                                                        {
                                                            name_print = (!string.IsNullOrEmpty(cx["customizationOptions"]["skus"]["name_print"].ToString()) ? cx["customizationOptions"]["skus"]["name_print"].ToString() : cx["customizationOptions"]["name"].ToString());
                                                        }

                                                        if ((name_print.Count<char>(new Func<char, bool>(char.IsWhiteSpace)) <= 3 ? false : name_print.Length > 30))
                                                        {
                                                            name_print = this.SplitString(name_print);
                                                        }
                                                    }
                                                    
                                                    int qty = mainQty * int.Parse(cx["quantity"].ToString());
                                                    kp[qx].Add(string.Concat(new object[] { "      ", qty, " x ", this.SplitString(name_print) }));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception exce)
                        {
                            var LineNumber = new StackTrace(exce, true).GetFrame(0).GetFileLineNumber();

                            Console.WriteLine("Error at line :" + LineNumber + " ==>" + exce.Message + "\n");
                        }

                        //processing customization option
                        foreach (JToken b in (IEnumerable<JToken>)a["orderCustomizationItemDetails"])
                        {
                            foreach (JToken c in (IEnumerable<JToken>)b["orderCustomizationOptionItemDetails"])
                            {
                                try
                                {

                                    var skus = String.Empty;
                                    
                                    try
                                    {
                                        //be aware with this  
                                        var customization = JObject.Parse(c["customizationOptions"].ToString());
                                        
                                        if (customization.ContainsKey("skus"))
                                        {
                                            
                                            skus = "   --" + c["customizationOptions"]["skus"]["name"].ToString();
                                        }
                                        else
                                        {
                                            
                                            skus = "   --" + c["customizationOptions"]["name"].ToString();
                                            
                                        }
                                        

                                    }
                                    catch (Exception tt)
                                    {
                                        var LineNumber = new StackTrace(tt, true).GetFrame(0).GetFileLineNumber();

                                        Console.WriteLine("Error at line :" + LineNumber + " ==>" + tt.Message + "\n");
                                        

                                    }

                                    if (c["customizationOptions"]["printers"].Count<JToken>() > 0)
                                    {
                                        foreach (JToken j in (IEnumerable<JToken>)c["customizationOptions"]["printers"])
                                        {
                                            for (int qx = 0; qx < count_printers; qx++)
                                            {
                                                if (j["name"].ToString() == kp[qx][0] && c["customizationOptions"]["send_as_item"].ToString() != "true")
                                                {
                                                    var CustSkus = JObject.Parse(c["customizationOptions"].ToString());

                                                    if (CustSkus.ContainsKey("skus"))
                                                    {
                                                        if (c["customizationOptions"]["skus"].Contains("name_print"))
                                                        {
                                                            if (!string.IsNullOrEmpty(c["customizationOptions"]["skus"]["name_print"].ToString())){
                                                                //name_print = c["customizationOptions"]["skus"]["name_print"].ToString() + skus;
                                                                name_print = c["customizationOptions"]["skus"]["name_print"].ToString() ;
                                                            }
                                                            else
                                                            {
                                                                name_print = c["customizationOptions"]["name"].ToString();
                                                            }
                                                            
                                                        }
                                                        else
                                                        {
                                                            //name_print = String.Concat(c["customizationOptions"]["name"]," ", c["customizationOptions"]["skus"]["name"]);
                                                            name_print = c["customizationOptions"]["name"].ToString();

                                                        }

                                                    }
                                                    else
                                                    {
                                                        name_print = c["customizationOptions"]["name"].ToString();
                                                    }
                                                    
                                                    if ((name_print.Count<char>(new Func<char, bool>(char.IsWhiteSpace)) <= 3 ? false : name_print.Length > 30))
                                                    {
                                                        name_print = this.SplitString(name_print);
                                                    }

                                                    int qty = mainQty * int.Parse(c["quantity"].ToString());
                                                    kp[qx].Add(string.Concat(new object[] { "      ", qty, " x ", name_print }));
                                                }
                                            }
                                        }
                                    }

                                }
                                catch (Exception exc)
                                {
                                    var LineNumber = new StackTrace(exc, true).GetFrame(0).GetFileLineNumber();

                                    Console.WriteLine("Error at line :" + LineNumber + " ==>" + exc.Message + "\n");
                                }
                            }
                        }
                        

                        for (int bg1 = 0; bg1 < count_printers; bg1++)
                        {
                            this.printerName = kp[bg1][0];
                            cons.setheader(this.getHeaderDoket());
                            cons.setOrderNo(this.getQueue());

                            String Order = cons.ordeNo;

                            if (IS_BUZZER)
                            {
                                if (FIELD_TABLE)
                                {
                                    Order += " - " + this.getBuzzerNo();
                                }
                                else
                                {
                                    Order += " - " + this.getTableNo();
                                }
                                
                            }

                            Kitchen kp1 = new Kitchen()
                            {
                                Header = cons.header,
                                OrderNo = Order
                            };

                            kp1.Content = kp[bg1];

                            string output = JsonConvert.SerializeObject(kp1);
                            try
                            {
                                if (!this.IsNullOrEmpty(kp[bg1].ToArray()) && kp[bg1].Count<string>() > 1)
                                {
                                    File.WriteAllText(string.Concat(new string[] { "C:/log/printer/KITCHEN/", kp[bg1][0], "/", this.getQueue(), ".json" }), output);
                                }
                                Thread.Sleep(200);
                            }
                            catch (Exception exception2)
                            {
                                var LineNumber = new StackTrace(exception2, true).GetFrame(0).GetFileLineNumber();

                                Console.WriteLine("Error at line :" + LineNumber + " ==>" + exception2.Message + "\n");
                            }
                        }

                    }
                    
                }
                else
                {
                    _logger.Info("Invalid Data Format");
                    Console.WriteLine("Invalid Data Format");
                }

                

            }
            catch (Exception u)
            {
                var LineNumber = new StackTrace(u, true).GetFrame(0).GetFileLineNumber();

                Console.WriteLine("Error at line :" + LineNumber + " ==>" + u.Message+"\n");

                
            }


        }

        public void SplitReceiptEmenu(JObject json)
        {
            try
            {
                this.fileReceipt = json;
                List<string> kitchcens = new List<string>();
                List<string> Contents = new List<string>();
                List<string> Printers = new List<string>();
                string dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string merchant = (string.IsNullOrEmpty(this.fileReceipt["merchant"]["name"].ToString()) ? "-" : this.fileReceipt["merchant"]["name"].ToString());
                string address = (string.IsNullOrEmpty(this.fileReceipt["merchant"]["address"].ToString()) ? "-" : this.fileReceipt["merchant"]["address"].ToString());
                string phone = (string.IsNullOrEmpty(this.fileReceipt["merchant"]["phone"].ToString()) ? "-" : this.fileReceipt["merchant"]["phone"].ToString());
                string queue = (string.IsNullOrEmpty(this.fileReceipt["orders"]["queue_no"].ToString()) ? "-" : this.fileReceipt["orders"]["queue_no"].ToString());
                string buzzerNo = (string.IsNullOrEmpty(this.fileReceipt["orders"]["buzzer_no"].ToString()) ? "-" : this.fileReceipt["orders"]["buzzer_no"].ToString());
                string terminal = (string.IsNullOrEmpty(this.fileReceipt["orders"]["terminal"].ToString()) ? "-" : this.fileReceipt["orders"]["terminal"].ToString());
                string tableNo = (string.IsNullOrEmpty(this.fileReceipt["orders"]["orderPosInfos"]["table_no"].ToString()) ? "-" : this.fileReceipt["orders"]["orderPosInfos"]["table_no"].ToString());
                string orderType = (string.IsNullOrEmpty(this.fileReceipt["orders"]["orderTypes"]["name"].ToString()) ? "-" : this.fileReceipt["orders"]["orderTypes"]["name"].ToString());
                this.fileReceipt["merchant"]["currency_type"].ToString();
                string OrderNo = queue;
                string header = string.Concat(new string[] { merchant, "\n", address, "\n", phone });
                string name_print = "";
                string filePrinter = "C:\\log\\printer\\printers.json";
                int n = 0;
                int cKp = 0;
                List<string>[] kp = new List<string>[1];
                using (StreamReader r = new StreamReader(filePrinter))
                {
                    JObject parsed = JObject.Parse(r.ReadToEnd());
                    r.Close();
                    cKp = parsed["data"].Count<JToken>();
                    kp = new List<string>[cKp];
                    foreach (JToken a in (IEnumerable<JToken>)parsed["data"])
                    {
                        kp[n] = new List<string>();
                        kp[n].Add(a.ToString());
                        n++;
                    }
                }
                int mainQty = 1;

                foreach (JToken a in (IEnumerable<JToken>)this.fileReceipt["orders"]["orderItemDetails"])
                {
                    mainQty = int.Parse(a["quantity"].ToString());
                    try
                    {

                        foreach (JToken b in (IEnumerable<JToken>)a["printers"])
                        {
                            for (int qx = 0; qx < cKp; qx++)
                            {

                                if (b["name"].ToString() == kp[qx][0])
                                {

                                    var skuName = String.Empty;
                                    try
                                    {
                                        skuName = a["skus"]["name_print"].ToString();
                                    }
                                    catch (Exception i)
                                    {

                                    }

                                    try
                                    {
                                        skuName = a["skus"][0]["name_print"].ToString();
                                    }
                                    catch (Exception i)
                                    {

                                    }

                                    name_print = (!string.IsNullOrEmpty(skuName) ? skuName : a["dish_name"].ToString());

                                    if (name_print == "NONE")
                                    {
                                        name_print = a["dish_name"].ToString();
                                    }
                                    if ((name_print.Count<char>(new Func<char, bool>(char.IsWhiteSpace)) <= 3 ? false : name_print.Length > 30))
                                    {
                                        name_print = this.SplitString(name_print);
                                    }


                                    kp[qx].Add(string.Concat(mainQty.ToString(), " x ", name_print));
                                    string request = a["special_instruction"].ToString();
                                    if (!string.IsNullOrEmpty(request))
                                    {
                                        if ((request.Count<char>(new Func<char, bool>(char.IsWhiteSpace)) <= 3 ? false : request.Length > 30))
                                        {
                                            request = this.SplitString(request);
                                        }
                                        kp[qx].Add(string.Concat("** Special Request : \n      ", request));
                                    }
                                    if (a["orderCustomizationItemDetails"].Count<JToken>() > 0)
                                    {
                                        foreach (JToken bx in (IEnumerable<JToken>)a["orderCustomizationItemDetails"])
                                        {
                                            if (bx["orderCustomizationOptionItemDetails"].Count<JToken>() <= 0)
                                            {
                                                continue;
                                            }
                                            foreach (JToken cx in (IEnumerable<JToken>)bx["orderCustomizationOptionItemDetails"])
                                            {
                                                if (cx["customizationOptions"]["send_as_item"].ToString() != "true")
                                                {
                                                    continue;
                                                }
                                                if (!cx["customizationOptions"]["sku"].Contains<JToken>("name_print"))
                                                {
                                                    name_print = cx["customizationOptions"]["sku"]["name"].ToString();
                                                }
                                                else
                                                {
                                                    name_print = (!string.IsNullOrEmpty(cx["customizationOptions"]["sku"]["name_print"].ToString()) ? cx["customizationOptions"]["sku"]["name_print"].ToString() : cx["customizationOptions"]["sku"]["name"].ToString());
                                                }
                                                if ((name_print.Count<char>(new Func<char, bool>(char.IsWhiteSpace)) <= 3 ? false : name_print.Length > 30))
                                                {
                                                    name_print = this.SplitString(name_print);
                                                }
                                                int qty = mainQty * int.Parse(cx["quantity"].ToString());
                                                kp[qx].Add(string.Concat(new object[] { "    ", qty, " x ", name_print }));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                    }

                    foreach (JToken b in (IEnumerable<JToken>)a["orderCustomizationItemDetails"])
                    {
                        foreach (JToken c in (IEnumerable<JToken>)b["orderCustomizationOptionItemDetails"])
                        {
                            try
                            {
                                if (c["customizationOptions"]["printers"].Count<JToken>() > 0)
                                {
                                    foreach (JToken j in (IEnumerable<JToken>)c["customizationOptions"]["printers"])
                                    {
                                        for (int qx = 0; qx < cKp; qx++)
                                        {
                                            if (j["name"].ToString() == kp[qx][0] && c["customizationOptions"]["send_as_item"].ToString() != "true")
                                            {
                                                if (!c["customizationOptions"]["sku"].Contains<JToken>("name_print"))
                                                {
                                                    name_print = c["customizationOptions"]["name"].ToString();
                                                }
                                                else
                                                {
                                                    name_print = (!string.IsNullOrEmpty(c["customizationOptions"]["sku"]["name_print"].ToString()) ? c["customizationOptions"]["sku"]["name_print"].ToString() : c["customizationOptions"]["name"].ToString());
                                                }
                                                if ((name_print.Count<char>(new Func<char, bool>(char.IsWhiteSpace)) <= 3 ? false : name_print.Length > 30))
                                                {
                                                    name_print = this.SplitString(name_print);
                                                }
                                                int qty = mainQty * int.Parse(c["quantity"].ToString());
                                                kp[qx].Add(string.Concat(new object[] { "    ", qty, " x ", name_print }));
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception exception1)
                            {
                            }
                        }
                    }
                }


                header = string.Concat("Table No            : ", tableNo, "\nOrder No            : ", OrderNo, "\nOrder type          : ", orderType, "\n");
                header = string.Concat(header, "Date & Time       : ", dt, "\n");

                Constant cons = new Constant();
                cons.setheader(header);
                cons.setOrderNo("");
                
                Kitchen kp1 = new Kitchen()
                {
                    Header = cons.header,
                    OrderNo = cons.ordeNo,
                };
                for (int bg1 = 0; bg1 < cKp; bg1++)
                {
                    kp1.Content = kp[bg1];
                    string output = JsonConvert.SerializeObject(kp1);
                    try
                    {
                        if (!this.IsNullOrEmpty(kp[bg1].ToArray()) && kp[bg1].Count<string>() > 1)
                        {
                            File.WriteAllText(string.Concat(new string[] { "C:/log/printer/", kp[bg1][0], "/", queue, ".json" }), output);
                        }
                        Thread.Sleep(200);
                    }
                    catch (Exception exception2)
                    {
                    }
                }

            }
            catch (Exception u)
            {
                //MessageBox.Show(u.Message);
            }


        }

        public string SplitString(string s)
        {
            StringBuilder sb = new StringBuilder(s);
            int spaces = 0;
            int length = sb.Length;
            int i = 0;
            while (i < length)
            {
                if (sb[i] == ' ')
                {
                    spaces++;
                }
                if (spaces != 3)
                {
                    i++;
                }
                else
                {
                    sb.Insert(i, string.Concat(Environment.NewLine, "     "));
                    break;
                }
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        [DllImport("TSCLIB.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern int windowsfont(int x, int y, int fontheight, int rotation, int fontstyle, int fontunderline, string szFaceName, string content);
    }
}