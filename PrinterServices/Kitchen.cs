﻿using System.Collections.Generic;

namespace PrinterServices
{
    internal class Kitchen
    {
        public string OrderNo { get; set; }
        
        public string Header { get; set; }

        public List<string> Content { get; set; }

    }
}