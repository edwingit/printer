﻿namespace PrinterServices
{
    internal class Constant
    {
        
        public const string BASE_PATH = "@Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)";
        public const string FILE_CONFIG = "C:\\log\\config.json";
        public const string FILE_LOG_PRINTERS = "C:\\log\\printer\\logs.json";
        public const string FILE_LOG_TERMINALS = "C:\\log\\terminals\\logs.json";
        public const string FILE_MASTER_PRINTERS = "C:\\log\\printer\\printers.json";
        public const string FILE_MASTER_IPS = "C:\\log\\printer\\ips.json";
        public const string IMG_PRINTER_STANDBY = @"images/printer_standby.jpg";
        public const string IMG_PRINTER_ON = @"images/printer.ico";

        public const int STD_WHITESPACE = 3;
        public const int STD_SPLIT_CHAR = 30;

        public string header;

        public string Merchant;

        public string ordeNo;

        public string buzzerNo;

        public string printerName;

        public void setMerchant(string s)
        {
            Merchant = s;
        }

        public string getMerchant()
        {
            return Merchant;
        }

        public void setheader(string s)
        {
            header = s;
        }

        public string getheader()
        {
            return header;
        }

        public void setOrderNo(string s)
        {
            ordeNo = s;
        }

        public string getOrderNo()
        {
            return ordeNo;
        }

        public void setBuzzerNo(string s)
        {
            buzzerNo = s;
        }

        public string getBuzzerNo()
        {
            return buzzerNo;
        }

        public void setPrinterName(string s)
        {
            printerName = s;
        }

        public string getPrinterName()
        {
            return printerName;
        }


    }
}